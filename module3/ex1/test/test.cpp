#include <vector>
#include <deque>
#include <queue>
#include <set>
#include <ostream>
#include <iostream>
#include <functional>
#include <sstream>

#include "igraph.h"
#include "list_graph.h"
#include "arc_graph.h"
#include "set_graph.h"
#include "matrix_graph.h"

namespace test
{
    using namespace std;
    // Define the number of runs for the test data
    // generated
#define RUN 1

    // Define the maximum number of vertices of the graph
#define MAX_VERTICES 50

    // Define the maximum number of edges
#define MAX_EDGES 1000

    void generate_test_data(std::ostream &out) {
        set<pair<int, int>> container;
        set<pair<int, int>>::iterator it;

        // Uncomment the below line to store
        // the test data in a file
        // freopen("Test_Cases_Undirected_Weighted_Graph.in",
        //          "w", stdout);

        //For random values every time
        srand(time(NULL));

        for (int i=1; i<=RUN; i++)
        {
            int NUM = 1 + rand() % MAX_VERTICES;    // Number of Vertices

            // Define the maximum number of edges of the graph
            // Since the most dense graph can have N*(N-1)/2 edges
            // where N =  nnumber of vertices in the graph
            int NUMEDGE = 1 + rand() % MAX_EDGES; // Number of Edges

            while (NUMEDGE > NUM*(NUM-1)/2)
                NUMEDGE = 1 + rand() % MAX_EDGES;

            // First print the number of vertices and edges
            out << NUM << endl;
            out << NUMEDGE << endl;

            // Then print the edges of the form (a b)
            // where 'a' is connected to 'b'
            for (int j=1; j<=NUMEDGE; j++)
            {
                int a = rand() % NUM;
                int b = rand() % NUM;
                pair<int, int> p = make_pair(a, b);
                pair<int, int> reverse_p = make_pair(b, a);

                // Search for a random "new" edge everytime
                // Note - In a tree the edge (a, b) is same
                // as the edge (b, a)
                while (container.find(p) != container.end() ||
                       container.find(reverse_p) != container.end())
                {
                    a = rand() % NUM;
                    b = rand() % NUM;
                    p = make_pair(a, b);
                    reverse_p = make_pair(b, a);
                }
                container.insert(p);
            }

            for (it = container.begin(); it != container.end(); ++it)
                out << it->first << " " << it->second << endl;

            container.clear();
            out << endl;
        }
    }
}



void BFS(const IGraph &graph, int vertex, std::vector<bool> &visited, const std::function<void(int)> &func)
{
    std::queue<int> qu;
    qu.push(vertex);
    visited[vertex] = true;

    while (!qu.empty())
    {
        int current_vertex = qu.front();
        qu.pop();

        func(current_vertex);

        for (int next_vertex: graph.get_next_vertices(current_vertex))
        {
            if (!visited[next_vertex])
            {
                qu.push(next_vertex);
                visited[next_vertex] = true;
            }
        }
    }
}

void mainBFS(const IGraph &graph, const std::function<void(int)> &func)
{
    std::vector<bool> visited(graph.vertices_count(), false);

    for (std::size_t vertex = 0; vertex < graph.vertices_count(); vertex++)
    {
        if (!visited[vertex])
        {
            BFS(graph, vertex, visited, func);
        }
    }
}


void topological_sort_internal(const IGraph &graph, int vertex, std::vector<bool> &visited, std::deque<int> &sorted)
{
    visited[vertex] = true;
    for (std::size_t next_vertex: graph.get_next_vertices(vertex))
    {
        if (!visited[next_vertex])
        {
            topological_sort_internal(graph, next_vertex, visited, sorted);
        }
    }
    sorted.push_front(vertex);
}

std::deque<int> topological_sort(const IGraph &graph)
{
    std::vector<bool> visited(graph.vertices_count(), false);
    std::deque<int> sorted;

    for (std::size_t vertex = 0; vertex < graph.vertices_count(); vertex++)
    {
        if (!visited[vertex])
        {
            topological_sort_internal(graph, vertex, visited, sorted);
        }
    }

    return sorted;
}

void print(std::ostream &out, const std::string& name, const IGraph &graph)
{
    out << name << std::endl;
    mainBFS(graph, [&out](std::size_t vertex) { out << vertex << " ";});
    out << std::endl;

    for (int i : topological_sort(graph))
        out << i << " ";
    out << std::endl;
}

int main()
{
    std::stringstream ss;
    test::generate_test_data(ss);

    std::cout << ss.str();

    std::cout.flush();

    std::size_t vert_count, edges_count;
    ss >> vert_count >> edges_count;

    std::size_t from, to;

    ListGraph main_graph(vert_count);
    for (std::size_t i = 0; i < edges_count; ++i)
    {
        ss >> from >> to;

        main_graph.add_edge(from, to);
        main_graph.add_edge(to, from);
    }

    print(std::cout, "main main_graph (list main_graph)", main_graph);

    ListGraph list_graph(main_graph);
    print(std::cout, "list_graph", list_graph);

    MatrixGraph matrix_graph(main_graph);
    print(std::cout, "matrix_graph", matrix_graph);

    SetGraph set_graph(matrix_graph);
    print(std::cout, "set_graph", set_graph);

    ArcGraph arc_graph(set_graph);
    print(std::cout, "arc_graph", arc_graph);

    std::vector<IGraph*> graphs_arr = {&list_graph, &matrix_graph, &set_graph, &arc_graph};

    for (std::size_t i = 0; i < main_graph.vertices_count(); ++i)
    {
        auto next_vertexes = main_graph.get_next_vertices(i);
        std::sort(next_vertexes.begin(), next_vertexes.end());

        for (const IGraph *curr_graph : graphs_arr)
        {
            auto curr_next_vertexes = curr_graph->get_next_vertices(i);
            std::sort(curr_next_vertexes.begin(), curr_next_vertexes.end());

            if (next_vertexes != curr_next_vertexes)
            {
                const auto func = [](auto &elem) {
                    std::cerr << elem << " ";
                };

                std::cerr << "Graphs not equal" << std::endl;

                std::for_each(next_vertexes.cbegin(), next_vertexes.cend(), func);
                std::cerr << std::endl;

                std::for_each(curr_next_vertexes.cbegin(), curr_next_vertexes.cend(), func);
                std::cerr << std::endl;
            }
        }
    }

    for (std::size_t i = 0; i < main_graph.vertices_count(); ++i)
    {
        auto next_vertexes = main_graph.get_next_vertices(i);
        std::sort(next_vertexes.begin(), next_vertexes.end());

        for (const IGraph *curr_graph : graphs_arr)
        {
            auto curr_next_vertexes = curr_graph->get_prev_vertices(i);
            std::sort(curr_next_vertexes.begin(), curr_next_vertexes.end());
            if (next_vertexes != curr_next_vertexes)
            {
                const auto func = [](auto &elem) {
                    std::cerr << elem << " ";
                };

                std::cerr << "Graphs not equal" << std::endl;

                std::for_each(next_vertexes.cbegin(), next_vertexes.cend(), func);
                std::cerr << std::endl;

                std::for_each(curr_next_vertexes.cbegin(), curr_next_vertexes.cend(), func);
                std::cerr << std::endl;
            }
        }
    }

    return 0;
}
