#include "set_graph.h"

SetGraph::SetGraph(std::size_t vert_count)
	: _vertices_count(vert_count)
	, _graph(_vertices_count) {}

SetGraph::SetGraph(const IGraph &graph)
	: _vertices_count(graph.vertices_count())
	, _graph(graph.vertices_count())
{
	for (std::size_t i = 0; i < _vertices_count; ++i)
	{
		std::vector<std::size_t> next_vert = graph.get_next_vertices(i);
		for (const std::size_t vert : next_vert)
		{
			add_edge(i, vert);
		}
	}
}

void SetGraph::add_edge(std::size_t from, std::size_t to)
{
	_graph[from].emplace(to);
}

std::size_t SetGraph::vertices_count() const noexcept
{
	return _vertices_count;
}

std::vector<std::size_t> SetGraph::get_next_vertices(std::size_t vertex) const
{
	return {_graph[vertex].cbegin(), _graph[vertex].cend()};
}

std::vector<std::size_t> SetGraph::get_prev_vertices(std::size_t vertex) const
{
    std::size_t i = 0;
	std::vector<std::size_t> prev_vert;

	for (const auto &next_vert : _graph)
	{
		for (const std::size_t curr_vert : next_vert)
		{
			if (curr_vert == vertex)
			{
				prev_vert.emplace_back(i);
			}
		}
		i++;
	}

	return prev_vert;
}
