#include "arc_graph.h"

ArcGraph::ArcGraph(std::size_t vert_count)
	: _vertices_count(vert_count)
	, _graph() {}

ArcGraph::ArcGraph(const IGraph &graph)
	: _vertices_count(graph.vertices_count())
	, _graph()
{
	for (std::size_t i = 0; i < _vertices_count; ++i) 
	{
        std::vector<std::size_t> next_vertices = graph.get_next_vertices(i);
        for (const std::size_t vert : next_vertices) 
        {
            add_edge(i, vert);
        }
    }
}

void ArcGraph::add_edge(std::size_t from, std::size_t to)
{
	_graph.emplace_back(from, to);
}

std::size_t ArcGraph::vertices_count() const noexcept
{
	return _vertices_count;
}

std::vector<std::size_t> ArcGraph::get_next_vertices(std::size_t vertex) const
{
	std::vector<std::size_t> next_vert;

	for (const auto &pair : _graph)
	{
		if (pair.first == vertex)
		{
			next_vert.push_back(pair.second);
		}
	}

	return next_vert;
}

std::vector<std::size_t> ArcGraph::get_prev_vertices(std::size_t vertex) const
{
	std::vector<std::size_t> prev_vert;

	for (const auto &pair : _graph)
	{
		if (pair.second == vertex)
		{
			prev_vert.push_back(pair.first);
		}
	}

	return prev_vert;
}
