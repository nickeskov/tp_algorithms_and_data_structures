#include "list_graph.h"

#include <exception>
#include <algorithm>


ListGraph::ListGraph(std::size_t vert_count)
	: _vertices_count(vert_count)
	, _graph(vert_count) {}


ListGraph::ListGraph(const IGraph &graph)
	: _vertices_count(graph.vertices_count())
	, _graph(_vertices_count)
{
	for (std::size_t i = 0; i < _vertices_count; ++i)
	{
		std::vector<std::size_t> next_vertices = graph.get_next_vertices(i);
		for (const std::size_t vert : next_vertices)
		{
			add_edge(i, vert);
		}
	}
}

void ListGraph::add_edge(std::size_t from, std::size_t to)
{
	if (to >= _graph.size())
	{
		_graph.emplace_back();
	}

	std::vector<std::size_t> &from_vert = _graph[from];

	if (std::find(from_vert.cbegin(), from_vert.cend(), to) == from_vert.cend())
	{
		from_vert.emplace_back(to);
	}
}

std::size_t ListGraph::vertices_count() const noexcept
{
	return _vertices_count;
}

std::vector<std::size_t> ListGraph::get_next_vertices(std::size_t vertex) const
{
	return _graph[vertex];
}

std::vector<std::size_t> ListGraph::get_prev_vertices(std::size_t vertex) const
{
    std::size_t i = 0;
	std::vector<std::size_t> prev_vert;

	for (const std::vector<std::size_t> &next_vert : _graph)
	{
		for (const std::size_t curr_vert : next_vert)
		{
			if (curr_vert == vertex)
			{
				prev_vert.emplace_back(i);
			}
		}
		i++;
	}

	return prev_vert;
}
