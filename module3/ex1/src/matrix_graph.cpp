#include "matrix_graph.h"

MatrixGraph::MatrixGraph(std::size_t vert_count)
	: _vertices_count(vert_count)
	, _graph(_vertices_count, std::vector<bool>(_vertices_count, false)) {}

MatrixGraph::MatrixGraph(const IGraph &graph)
	: _vertices_count(graph.vertices_count())
	, _graph(_vertices_count, std::vector<bool>(_vertices_count, false))
{
	for (std::size_t i = 0; i < _vertices_count; ++i) 
	{
        std::vector<std::size_t> next_vertices = graph.get_next_vertices(i);
        for (const std::size_t vert : next_vertices) 
        {
            add_edge(i, vert);
        }
    }
}

void MatrixGraph::add_edge(std::size_t from, std::size_t to)
{
	_graph[from][to] = true;
}

std::size_t MatrixGraph::vertices_count() const noexcept
{
	return _vertices_count;
}

std::vector<std::size_t> MatrixGraph::get_next_vertices(std::size_t vertex) const
{
	std::vector<std::size_t> next_vert;

	for (std::size_t i = 0; i < _vertices_count; ++i)
	{
		if (_graph[vertex][i])
		{
			next_vert.push_back(i);
		}
	}

	return next_vert;
}

std::vector<std::size_t> MatrixGraph::get_prev_vertices(std::size_t vertex) const
{
	std::vector<std::size_t> prev_vert;

	for (std::size_t i = 0; i < _vertices_count; ++i)
	{
		if (_graph[i][vertex])
		{
			prev_vert.push_back(i);
		}
	}

	return prev_vert;
}
