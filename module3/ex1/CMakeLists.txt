cmake_minimum_required(VERSION 3.6)

project(ex1)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(GCC_COVERAGE_COMPILE_FLAGS "--coverage -fprofile-arcs -ftest-coverage -fPIC")
set(GCC_COVERAGE_LINK_FLAGS "-lgcov --coverage")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS} \
                        ${GCC_COVERAGE_LINK_FLAGS} \
                        -std=c++11 -Wall -Wextra -Werror -ggdb3")

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS} \
                    ${GCC_COVERAGE_LINK_FLAGS} \
                    -O0 -std=c11 -Wall -Wextra -Werror -ggdb3")

# ------------------------------------------------------------------------------
# Includes
# ------------------------------------------------------------------------------

include_directories(include)

# ------------------------------------------------------------------------------

# if(ENABLE_CPPCHECK)

    list(APPEND CPPCHECK_ARGS
        --check-library
        --enable=warning,style,performance,portability #,unusedFunction
        --std=c++11
        --verbose
        --error-exitcode=1
        --language=c++
        -I ${CMAKE_SOURCE_DIR}/include
        ${CMAKE_SOURCE_DIR}/include/*.h*
        ${CMAKE_SOURCE_DIR}/src/*.c*
        ${CMAKE_SOURCE_DIR}/test/*.c*
    )

    add_custom_target(
        check
        COMMENT "running cppcheck"
        COMMAND cppcheck ${CPPCHECK_ARGS}
    )

# endif()

execute_process (
   COMMAND sh -c "\
       find '${CMAKE_SOURCE_DIR}' -type f \
            -not -path '${CMAKE_SOURCE_DIR}/*build*/*' \
            -and -not -path '${CMAKE_SOURCE_DIR}/*test*/*' \
            -name '*.c??' \
       | tr '\n' ' '\
    "
   OUTPUT_VARIABLE LIB_SRC
   OUTPUT_STRIP_TRAILING_WHITESPACE
)

SEPARATE_ARGUMENTS(LIB_SRC)

#message(${LIB_SRC})

add_library(graphs STATIC ${LIB_SRC})

add_executable(tests.out test/test.cpp)

 target_link_libraries(tests.out graphs)
