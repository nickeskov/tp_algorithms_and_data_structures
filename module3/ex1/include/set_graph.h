#ifndef SET_GRAPH_H
#define SET_GRAPH_H

#include <vector>
#include <unordered_set>

#include "igraph.h"

class SetGraph : public IGraph
{
  public:

	explicit SetGraph(std::size_t vert_count);

  	explicit SetGraph(const IGraph &graph);

	void add_edge(std::size_t from, std::size_t to) override;

	[[nodiscard]] std::size_t vertices_count() const noexcept override;

	[[nodiscard]] std::vector<std::size_t> get_next_vertices(std::size_t vertex) const override;
	
	[[nodiscard]] std::vector<std::size_t> get_prev_vertices(std::size_t vertex) const override;

	~SetGraph() override = default;

  private:
  	std::size_t	_vertices_count;
  	std::vector<std::unordered_set<std::size_t>> _graph;
};

#endif
