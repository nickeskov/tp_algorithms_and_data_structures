#ifndef ARC_GRAPH_H
#define ARC_GRAPH_H

#include <utility>
#include <vector>

#include "igraph.h"


class ArcGraph : public IGraph
{
  public:

	explicit ArcGraph(std::size_t vert_count);

  	explicit ArcGraph(const IGraph &graph);

	void add_edge(std::size_t from, std::size_t to) override;

	[[nodiscard]] std::size_t vertices_count() const noexcept override;

	[[nodiscard]] std::vector<std::size_t> get_next_vertices(std::size_t vertex) const override;
	
	[[nodiscard]] std::vector<std::size_t> get_prev_vertices(std::size_t vertex) const override;

	~ArcGraph() override = default;

  private:
  	std::size_t	_vertices_count;
  	std::vector<std::pair<std::size_t, std::size_t>> _graph;
};

#endif
