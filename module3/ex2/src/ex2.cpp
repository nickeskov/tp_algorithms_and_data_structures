#include <iostream>
#include <limits>
#include <vector>
#include <queue>

#include "list_graph.h"

std::size_t num_shortest_path(const IGraph &graph, std::size_t from, std::size_t to)
{
    std::vector<std::size_t> dist(graph.vertices_count(), std::numeric_limits<std::size_t>::max());
    dist[from] = 0;

    std::vector<std::size_t> path_num(graph.vertices_count(), 0);
    path_num[from] = 1;

    std::queue<std::size_t> queue;
    queue.push(from);

    while (!queue.empty())
    {
        std::size_t curr_vert = queue.front();
        queue.pop();

        std::vector<std::size_t> next_vertexes = graph.get_next_vertices(curr_vert);

        for (std::size_t next_vertex : next_vertexes)
        {
            if (dist[next_vertex] == std::numeric_limits<decltype(next_vertex)>::max())
            {
                dist[next_vertex] = dist[curr_vert] + 1;
                path_num[next_vertex] = path_num[curr_vert];
                queue.push(next_vertex);
            }
            else if (dist[next_vertex] == dist[curr_vert] + 1)
            {
                path_num[next_vertex] += path_num[curr_vert];
            }
        }
    }
    return path_num[to];
}


int main()
{
    std::size_t vert_count, edges_count;
    std::cin >> vert_count >> edges_count;

    std::size_t from, to;

    ListGraph graph(vert_count);
    for (std::size_t i = 0; i < edges_count; ++i)
    {
        std::cin >> from >> to;
        graph.add_edge(from, to);
        graph.add_edge(to, from);
    }

    std::cin >> from >> to;
    std::cout << num_shortest_path(graph, from, to) << std::endl;

    return 0;
}
