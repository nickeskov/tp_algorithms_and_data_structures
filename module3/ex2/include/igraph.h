#ifndef IGRAPH_H
#define IGRAPH_H

#include <vector>

class IGraph {
  public:
	// Добавление ребра от from к to.
	virtual void add_edge(std::size_t from, std::size_t to) = 0;

	[[nodiscard]] virtual std::size_t vertices_count() const noexcept = 0;

	[[nodiscard]] virtual std::vector<std::size_t> get_next_vertices(std::size_t vertex) const = 0;

	[[nodiscard]] virtual std::vector<std::size_t> get_prev_vertices(std::size_t vertex) const = 0;

	virtual ~IGraph() = default;
};

#endif
