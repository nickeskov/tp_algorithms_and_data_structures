#ifndef LIST_GRAPH_H
#define LIST_GRAPH_H

#include <vector>

#include "igraph.h"


class ListGraph : public IGraph
{
  public:

	explicit ListGraph(std::size_t vert_count);

  	explicit ListGraph(const IGraph &graph);

	void add_edge(std::size_t from, std::size_t to) override;

	[[nodiscard]] std::size_t vertices_count() const noexcept override;

	[[nodiscard]] std::vector<std::size_t> get_next_vertices(std::size_t vertex) const override;
	
	[[nodiscard]] std::vector<std::size_t> get_prev_vertices(std::size_t vertex) const override;

	~ListGraph() override = default;

  private:
  	std::size_t	_vertices_count;
  	std::vector<std::vector<std::size_t>> _graph;
};

#endif
