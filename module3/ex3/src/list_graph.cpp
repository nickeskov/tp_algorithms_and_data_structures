#include "list_graph.h"

#include <exception>
#include <algorithm>
#include <functional>

ListGraph::ListGraph(std::size_t vert_count)
        : _vertices_count(vert_count)
        , _graph(vert_count) {}

void ListGraph::add_edge(std::size_t from, std::size_t to, std::size_t weight) {
    if (to >= _graph.size())
    {
        _graph.emplace_back();
    }

    auto &from_vert = _graph[from];

    if (std::find(from_vert.cbegin(), from_vert.cend(),
            std::make_pair(to, weight)) == from_vert.cend())
    {
        from_vert.emplace_back(to, weight);
    }
//    else
//    {
//        throw std::logic_error("'to' already in 'from'");
//    }
}

std::size_t ListGraph::vertices_count() const noexcept
{
    return _vertices_count;
}

std::vector<std::pair<std::size_t, std::size_t>> ListGraph::get_next_vertices(std::size_t vertex) const
{
    return _graph[vertex];
}
