#include <iostream>
#include <utility>
#include <limits>
#include <vector>
#include <set>
#include <type_traits>

#include "list_graph.h"


//#define A_STAR
#define DIJKSTRA

std::size_t shortest_way_len(const ListGraph &graph, std::size_t from, std::size_t to)
{
    std::vector<std::size_t> dist(graph.vertices_count(), std::numeric_limits<std::size_t>::max());
    dist[from] = 0;

    std::set<std::pair<std::size_t, std::size_t>> set;
    set.emplace(dist[from], from);

    while (!set.empty())
    {
        auto curr = set.extract(set.begin()).value().second;
        auto next_vertexes = graph.get_next_vertices(curr);
        for (const auto &[next, weight] : next_vertexes)
        {
#ifdef A_STAR
            // a-star algorithm
            if (dist[next] == std::numeric_limits<std::remove_reference<decltype(dist[next])>::type>::max())
            {
                set.emplace(dist[next], next);
            }
            if (dist[next] > dist[curr] + weight)
            {
                set.erase(std::make_pair(dist[next], next));
                dist[next] = dist[curr] + weight;
                set.emplace(dist[next], next);
            }
#undef DIJKSTRA
#endif
#ifdef DIJKSTRA
            // dijkstra algorithm
            if (dist[next] > dist[curr] + weight)
            {
                if (dist[next] != std::numeric_limits<std::remove_reference<decltype(dist[next])>::type>::max())
                {
                    set.erase(std::make_pair(dist[next], next));
                }
                dist[next] = dist[curr] + weight;
                set.emplace(dist[next], next);
            }
#endif
        }
    }

    return dist[to];
}

int main() {
    std::size_t vert_count, edges_count;
    std::cin >> vert_count >> edges_count;

    std::size_t from, to;

    ListGraph graph(vert_count);
    for (std::size_t i = 0; i < edges_count; ++i)
    {
        std::size_t weight;
        std::cin >> from >> to >> weight;

        graph.add_edge(from, to, weight);
        graph.add_edge(to, from, weight);
    }

    std::cin >> from >> to;
    std::cout << shortest_way_len(graph, from, to) << std::endl;

    return 0;
}
