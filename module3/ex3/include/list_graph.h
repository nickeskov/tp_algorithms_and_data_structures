#ifndef LIST_GRAPH_H
#define LIST_GRAPH_H

#include <utility>
#include <vector>

class ListGraph
{
public:

    explicit ListGraph(std::size_t vert_count);

    void add_edge(std::size_t from, std::size_t to, std::size_t weight);

    [[nodiscard]] std::size_t vertices_count() const noexcept;

    [[nodiscard]] std::vector<std::pair<std::size_t, std::size_t>>
        get_next_vertices(std::size_t vertex) const;

private:
    std::size_t	_vertices_count;
    std::vector<std::vector<std::pair<std::size_t, std::size_t>>> _graph;
};

#endif
