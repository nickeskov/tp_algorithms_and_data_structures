template <typename T>
size_t  binary_search(const T* arr, size_t size, T& element)
{
    if (arr == nullptr || size == 0)
        return ((size_t) -1);

    size_t first = 0;
    size_t last = size;

    while (first < last)
    {
        size_t middle = (first + last) / 2;
        if (arr[middle] < element)
            first = ++middle;
        else
            last = middle;
    }
    return ((first == size || arr[first] != element) ? ((size_t) -1) : first);
}
