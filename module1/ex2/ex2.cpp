#include <iostream>

#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define ABS(x) ((x) < 0 ? -(x): (x))

template <typename T>
static size_t  closest_search(const T* arr, T& key,
                                size_t start, size_t end)
{
    long first = start;
    long last = end;

    while (first < last)
    {
        long middle = (first + last) / 2;
        if (arr[middle] < key)
            first = ++middle;
        else
            last = middle;
    }

    if (first > start && ABS(arr[first - 1] - key) <= ABS(arr[first] - key))
        --first;
    return (MIN(first,end - 1));
}

template <typename T>
static size_t  exponential_search(const T* arr, size_t size, T& key)
{
    if (arr == nullptr || size == 0)
        return ((size_t) -1);

    size_t bound = 1;
    while (bound < size && arr[bound] < key)
        bound *= 2;
    return (closest_search(arr, key, bound / 2, MIN(bound + 1, size)));
}

template <typename T>
static void     init_arr(T* arr, size_t n)
{
    for (size_t i = 0; i < n; ++i)
    {
        std::cin >> *arr;
        ++arr;
    }
}

int             main(void)
{
    size_t  n, m;

    std::cin >> n;
    if (n == 0)
        return (1);
    int *a_arr = new int[n];
    init_arr(a_arr, n);

    std::cin >> m;
    if (m == 0)
        return (1);
    int *b_arr = new int[m];
    init_arr(b_arr, m);

    for (size_t i = 0; i < m - 1; ++i)
        std::cout << exponential_search(a_arr, n, b_arr[i]) << " ";
    std::cout << exponential_search(a_arr, n, b_arr[m - 1]) << std::endl;

    delete[] a_arr;
    delete[] b_arr;

    return (0);
}
