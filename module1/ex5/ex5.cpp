#include <iostream>
#include <cstddef>

#include "ft_vector.h"
#include "merge_sort.h"
#include "life_event.h"


int         main(void)
{
    size_t n;
    LifeEvent::DateComparator<int> cmp;

    std::cin >> n;
    auto vector = FtVector<LifeEvent::Date<int>>(n * 2);
    for (size_t i = 0; i < n; ++i)
    {
        auto born = LifeEvent::Date<int>(LifeEvent::Type::BORN);
        auto death = LifeEvent::Date<int>(LifeEvent::Type::DEATH);

        auto mature = born;
        mature.year += 18;

        if (!cmp(mature, death))
            continue ;

        auto old = born;
        old.year += 80;
        old.event = LifeEvent::Type::DEATH;

        if (cmp(death, old))
            old = death;

        vector.push_back(mature);
        vector.push_back(old);
    }

    merge_sort<LifeEvent::Date<int>, LifeEvent::DateComparator<int>>(vector);

    size_t max_persons = 0;
    size_t curr_persons = 0;

    for (size_t i = 0; i < vector.size(); ++i)
    {
        curr_persons += (vector[i].event == LifeEvent::Type::BORN) ? 1 : -1;
        if (curr_persons > max_persons)
            max_persons = curr_persons;
    }
    std::cout << max_persons << std::endl;
    return (0);
}
