#ifndef DEFAULT_COMPARATOR_H
#define DEFAULT_COMPARATOR_H

template <typename T>
class DefaultComparator
{
  public:
    bool operator()(const T& lhs, const T& rhs)
    {
        return (lhs < rhs);
    }
};

#endif /* DEFAULT_COMPARATOR_H */
#ifndef FT_VECTOR_H
# define FT_VECTOR_H

#include <algorithm>
#include <utility>
#include <stdexcept>

template <typename T>
class FtVector
{
  public:

    FtVector(): m_size(0), m_capacity(0), m_data(nullptr) {}

    explicit FtVector(std::size_t init_size)
        : m_size(0)
        , m_capacity(init_size)
        , m_data(init_size ? new T[init_size] : nullptr) {}

    FtVector(const FtVector& other)
        : m_size(other.m_size)
        , m_capacity(other.m_capacity)
        , m_data(other.m_capacity ? new T[other.m_capacity] : nullptr)

    {
        std::copy(other.begin(), other.end(), begin());
    }

    FtVector(FtVector&& other) noexcept
        : m_size(other.m_size)
        , m_capacity(other.m_capacity)
        , m_data(other.m_data)
    {
        other.m_size = 0;
        other.m_capacity = 0;
        other.m_data = nullptr;
    }

    FtVector&   operator=(const FtVector& other)
    {
        if (this == &other)
            return (*this);

        delete[] m_data;
        m_data = new T[other.m_capacity];
        m_capacity = other.m_capacity;
        m_size = other.m_size;
        std::copy(other.begin(), other.end(), begin());
        return (*this);
    }

    FtVector&   operator=(FtVector&& other) noexcept
    {
        if (this == &other)
            return (*this);

        delete[] m_data;
        m_size = other.m_size;
        m_capacity = other.m_capacity;
        m_data = other.m_data;

        other.m_size = 0;
        other.m_capacity = 0;
        other.m_data = nullptr;

        return (*this);
    }

    void        push_back(const T& elem)
    {
        if (m_size == m_capacity)
        {
            std::size_t new_capacity;
            new_capacity = (m_size ?
                m_size * mc_capacity_increase_rate :
                mc_capacity_increase_rate);
            reserve(new_capacity);
        }
        m_data[m_size] = elem;
        ++m_size;
    }

    void        push_back(T&& elem)
    {
        if (m_size == m_capacity)
        {
            std::size_t new_capacity;
            new_capacity = (m_size ?
                m_size * mc_capacity_increase_rate :
                mc_capacity_increase_rate);
            reserve(new_capacity);
        }
        m_data[m_size] = std::move(elem);
        ++m_size;
    }

    T&          front()
    {
        if (m_data && m_size)
            return (*m_data);
        else
            throw (std::out_of_range("front"));
    }

    const T&    front() const
    {
        if (m_data && m_size)
            return (*m_data);
        else
            throw (std::out_of_range("front"));
    }

    T&          back()
    {
        if (m_data && m_size)
            return (m_data[m_size - 1]);
        else
            throw (std::out_of_range("back"));
    }

    const T&    back() const
    {
        if (m_data && m_size)
            return (m_data[m_size - 1]);
        else
            throw (std::out_of_range("back"));
    }

    void        pop_back()
    {
        if (m_data && m_size)
            --m_size;
        else
            throw (std::out_of_range("pop_back"));
    }

    T&          at(std::size_t pos)
    {
        if (m_data && pos < m_size)
            return (m_data[pos]);
        else
            throw (std::out_of_range("at"));
    }

    const T&    at(std::size_t pos) const
    {
        if (m_data && pos < m_size)
            return (m_data[pos]);
        else
            throw (std::out_of_range("at"));
    }

    T&          operator[](std::size_t pos)
    {
        return (at(pos));
    }

    const T&    operator[](std::size_t pos) const
    {
        return (at(pos));
    }

    void        resize(std::size_t new_size)
    {
        const T value = T();
        resize(new_size, value);
    }

    void        resize(std::size_t new_size, const T& value)
    {
        reserve(new_size);

        for (size_t i = m_size; i < new_size; ++i)
            m_data[i] = value;

        m_size = new_size;
   }

    void        reserve(std::size_t new_capacity)
    {
        if (new_capacity > m_capacity)
        {
            T   *new_data = new T[new_capacity];

            std::copy(begin(), end(), new_data);
            delete[] m_data;

            m_capacity = new_capacity;
            m_data = new_data;
        }
    }

    std::size_t size() const noexcept
    {
        return (m_size);
    }

    bool        empty() const noexcept
    {
        return (m_size == 0);
    }

    T*          begin() noexcept
    {
        return (m_data);
    }

    const T*    begin() const noexcept
    {
        return (m_data);
    }

    T*          end() noexcept
    {
        return (m_data + m_size);
    }

    const T*    end() const noexcept
    {
        return (m_data + m_size);
    }

    T*          data() noexcept
    {
        return (m_data);
    }

    const T*    data() const noexcept
    {
        return (m_data);
    }

    void        clear() noexcept
    {
        m_size = 0;
    }

    void        shrink_to_fit()
    {
        if (m_size != m_capacity)
        {
            T   *new_data = new T[m_size];
            std::copy(begin(), end(), new_data);
            delete[] m_data;

            m_data = new_data;
            m_capacity = m_size;
        }
    }

    void        reverse()
    {
        if (!m_size)
            return ;

        size_t i = 0;
        size_t j = m_size - 1;

        while (i < j)
        {
            std::swap(m_data[i], m_data[j]);
            ++i;
            --j;
        }
    }

    virtual
    ~FtVector()
    {
        delete[] m_data;
        m_data = nullptr;
    }

    void        unsafe_resize(std::size_t new_size)
    {
        reserve(new_size);
        m_size = new_size;
    }

  private:

    std::size_t         m_size;
    std::size_t         m_capacity;
    T                   *m_data;
    const std::size_t   mc_capacity_increase_rate = 2;
};

#endif /* FT_VECTOR_H */
#ifndef LIFE_EVENT_H
# define LIFE_EVENT_H

# include <istream>
# include <iostream>

namespace LifeEvent
{
    enum class Type: unsigned int
    {
        NONE,
        BORN,
        DEATH
    };

    template <typename T = unsigned int>
    struct Date
    {
        Date() = default;

        explicit
        Date(Type event_type, std::istream& istream = std::cin)
            : event(event_type)
        {
            istream >> day >> month >> year;
        }

        T               day;
        T               month;
        T               year;
        LifeEvent::Type event = LifeEvent::Type::NONE;
    };

    template <typename T>
    class DateComparator
    {
      public:
        bool operator()(const Date<T>& lhs, const Date<T>& rhs)
        {
            if (lhs.year < rhs.year)
                return (true);
            if (lhs.year == rhs.year)
            {
                if (lhs.month < rhs.month)
                    return (true);
                if (lhs.month == rhs.month)
                {
                    if (lhs.day < rhs.day)
                        return (true);
                    if (lhs.day == rhs.day
                        && lhs.event == Type::DEATH)
                        return (true);
                }
            }
            return (false);
        }
    };

};

#endif /* LIFE_EVENT_H */
#ifndef MERGE_SORT_H
# define MERGE_SORT_H

# include <utility>
# include <algorithm>


template <typename T, typename Comparator = DefaultComparator<T>>
static void    merge(FtVector<T>& vector, size_t left, size_t mid, size_t right,
                Comparator cmp = Comparator())
{
    size_t      it1 = 0;
    size_t      it2 = 0;
    FtVector<T> result(right - left);

    while ((left + it1) < mid && (mid + it2) < right)
    {
        if (cmp(vector[left + it1], vector[mid + it2]))
        {
            result.push_back(std::move(vector[left + it1]));
            ++it1;
        }
        else
        {
            result.push_back(std::move(vector[mid + it2]));
            ++it2;
        }
    }

    while (left + it1 < mid)
    {
        result.push_back(std::move(vector[left + it1]));
        ++it1;
    }

    while (mid + it2 < right)
    {
        result.push_back(std::move(vector[mid + it2]));
        ++it2;
    }

    for (size_t i = 0; i < it1 + it2; ++i)
        vector[left + i] = std::move(result[i]);
}

template <typename T, typename Comparator = DefaultComparator<T>>
void        merge_sort_rec(FtVector<T>& vector, size_t left, size_t right,
                        Comparator& cmp = Comparator())
{
    if (left + 1 >= right)
        return ;

    const size_t mid = (left + right) / 2;

    merge_sort_rec(vector, left, mid, cmp);
    merge_sort_rec(vector, mid, right, cmp);

    merge(vector, left, mid, right, cmp);
}

template <typename T, typename Comparator = DefaultComparator<T>>
void        merge_sort(FtVector<T>& vector, Comparator cmp = Comparator())
{
    if (vector.size() < 2)
        return;

    const size_t size = vector.size();

    for (size_t i = 1; i < size; i *= 2)
        for (size_t j = 0; j < size - i; j += 2 * i)
            merge(vector, j, j + i, std::min(j + 2 * i, size), cmp);
}

#endif /* MERGE_SORT_H */
#include <iostream>
#include <cstddef>



int         main(void)
{
    size_t n;
    LifeEvent::DateComparator<int> cmp;

    std::cin >> n;
    auto vector = FtVector<LifeEvent::Date<int>>(n * 2);
    for (size_t i = 0; i < n; ++i)
    {
        auto born = LifeEvent::Date<int>(LifeEvent::Type::BORN);
        auto death = LifeEvent::Date<int>(LifeEvent::Type::DEATH);

        auto mature = born;
        mature.year += 18;

        if (!cmp(mature, death))
            continue ;

        auto old = born;
        old.year += 80;
        old.event = LifeEvent::Type::DEATH;

        if (cmp(death, old))
            old = death;

        vector.push_back(mature);
        vector.push_back(old);
    }

    merge_sort<LifeEvent::Date<int>, LifeEvent::DateComparator<int>>(vector);

    size_t max_persons = 0;
    size_t curr_persons = 0;

    for (size_t i = 0; i < vector.size(); ++i)
    {
        curr_persons += (vector[i].event == LifeEvent::Type::BORN) ? 1 : -1;
        if (curr_persons > max_persons)
            max_persons = curr_persons;
    }
    std::cout << max_persons << std::endl;
    return (0);
}
