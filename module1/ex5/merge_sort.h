#ifndef MERGE_SORT_H
# define MERGE_SORT_H

# include <utility>
# include <algorithm>

# include "default_comparator.h"
# include "ft_vector.h"

template <typename T, typename Comparator = DefaultComparator<T>>
static void    merge(FtVector<T>& vector, size_t left, size_t mid, size_t right,
                Comparator cmp = Comparator())
{
    size_t      it1 = 0;
    size_t      it2 = 0;
    FtVector<T> result(right - left);

    while ((left + it1) < mid && (mid + it2) < right)
    {
        if (cmp(vector[left + it1], vector[mid + it2]))
        {
            result.push_back(std::move(vector[left + it1]));
            ++it1;
        }
        else
        {
            result.push_back(std::move(vector[mid + it2]));
            ++it2;
        }
    }

    while (left + it1 < mid)
    {
        result.push_back(std::move(vector[left + it1]));
        ++it1;
    }

    while (mid + it2 < right)
    {
        result.push_back(std::move(vector[mid + it2]));
        ++it2;
    }

    for (size_t i = 0; i < it1 + it2; ++i)
        vector[left + i] = std::move(result[i]);
}

template <typename T, typename Comparator = DefaultComparator<T>>
void        merge_sort_rec(FtVector<T>& vector, size_t left, size_t right,
                        Comparator& cmp = Comparator())
{
    if (left + 1 >= right)
        return ;

    const size_t mid = (left + right) / 2;

    merge_sort_rec(vector, left, mid, cmp);
    merge_sort_rec(vector, mid, right, cmp);

    merge(vector, left, mid, right, cmp);
}

template <typename T, typename Comparator = DefaultComparator<T>>
void        merge_sort(FtVector<T>& vector, Comparator cmp = Comparator())
{
    if (vector.size() < 2)
        return;

    const size_t size = vector.size();

    for (size_t i = 1; i < size; i *= 2)
        for (size_t j = 0; j < size - i; j += 2 * i)
            merge(vector, j, j + i, std::min(j + 2 * i, size), cmp);
}

#endif /* MERGE_SORT_H */
