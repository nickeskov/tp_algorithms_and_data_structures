#ifndef LIFE_EVENT_H
# define LIFE_EVENT_H

# include <istream>
# include <iostream>

namespace LifeEvent
{
    enum class Type: unsigned int
    {
        NONE,
        BORN,
        DEATH
    };

    template <typename T = unsigned int>
    struct Date
    {
        Date() = default;

        explicit
        Date(Type event_type, std::istream& istream = std::cin)
            : event(event_type)
        {
            istream >> day >> month >> year;
        }

        T               day;
        T               month;
        T               year;
        LifeEvent::Type event = LifeEvent::Type::NONE;
    };

    template <typename T>
    class DateComparator
    {
      public:
        bool operator()(const Date<T>& lhs, const Date<T>& rhs)
        {
            if (lhs.year < rhs.year)
                return (true);
            if (lhs.year == rhs.year)
            {
                if (lhs.month < rhs.month)
                    return (true);
                if (lhs.month == rhs.month)
                {
                    if (lhs.day < rhs.day)
                        return (true);
                    if (lhs.day == rhs.day
                        && lhs.event == Type::DEATH)
                        return (true);
                }
            }
            return (false);
        }
    };

};

#endif /* LIFE_EVENT_H */
