#include <cstddef>
#include <iostream>

#include "ft_vector.h"
#include "order_statistics.h"

int             main(void)
{
    size_t n, k;

    std::cin >> n >> k;
    auto vector = FtVector<int>(n);

    for (size_t i = 0; i < n; ++i)
    {
        int tmp;
        std::cin >> tmp;
        vector.push_back(tmp);
    }

    std::cout << vector[order_statistic(vector, k)] << std::endl;
    return (0);
}
