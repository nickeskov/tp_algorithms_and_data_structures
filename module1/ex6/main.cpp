#ifndef DEFAULT_COMPARATOR_H
#define DEFAULT_COMPARATOR_H

template <typename T>
class DefaultComparator
{
  public:
    bool operator()(const T& lhs, const T& rhs)
    {
        return (lhs < rhs);
    }
};

#endif /* DEFAULT_COMPARATOR_H */
#ifndef FT_VECTOR_H
# define FT_VECTOR_H

#include <algorithm>
#include <utility>
#include <stdexcept>

template <typename T>
class FtVector
{
  public:

    FtVector(): m_size(0), m_capacity(0), m_data(nullptr) {}

    explicit FtVector(std::size_t init_size)
        : m_size(0)
        , m_capacity(init_size)
        , m_data(init_size ? new T[init_size] : nullptr) {}

    FtVector(const FtVector& other)
        : m_size(other.m_size)
        , m_capacity(other.m_capacity)
        , m_data(other.m_capacity ? new T[other.m_capacity] : nullptr)

    {
        std::copy(other.begin(), other.end(), begin());
    }

    FtVector(FtVector&& other) noexcept
        : m_size(other.m_size)
        , m_capacity(other.m_capacity)
        , m_data(other.m_data)
    {
        other.m_size = 0;
        other.m_capacity = 0;
        other.m_data = nullptr;
    }

    FtVector&   operator=(const FtVector& other)
    {
        if (this == &other)
            return (*this);

        delete[] m_data;
        m_data = new T[other.m_capacity];
        m_capacity = other.m_capacity;
        m_size = other.m_size;
        std::copy(other.begin(), other.end(), begin());
        return (*this);
    }

    FtVector&   operator=(FtVector&& other) noexcept
    {
        if (this == &other)
            return (*this);

        delete[] m_data;
        m_size = other.m_size;
        m_capacity = other.m_capacity;
        m_data = other.m_data;

        other.m_size = 0;
        other.m_capacity = 0;
        other.m_data = nullptr;

        return (*this);
    }

    void        push_back(const T& elem)
    {
        if (m_size == m_capacity)
        {
            std::size_t new_capacity;
            new_capacity = (m_size ?
                m_size * mc_capacity_increase_rate :
                mc_capacity_increase_rate);
            reserve(new_capacity);
        }
        m_data[m_size] = elem;
        ++m_size;
    }

    void        push_back(T&& elem)
    {
        if (m_size == m_capacity)
        {
            std::size_t new_capacity;
            new_capacity = (m_size ?
                m_size * mc_capacity_increase_rate :
                mc_capacity_increase_rate);
            reserve(new_capacity);
        }
        m_data[m_size] = std::move(elem);
        ++m_size;
    }

    T&          front()
    {
        if (m_data && m_size)
            return (*m_data);
        else
            throw (std::out_of_range("front"));
    }

    const T&    front() const
    {
        if (m_data && m_size)
            return (*m_data);
        else
            throw (std::out_of_range("front"));
    }

    T&          back()
    {
        if (m_data && m_size)
            return (m_data[m_size - 1]);
        else
            throw (std::out_of_range("back"));
    }

    const T&    back() const
    {
        if (m_data && m_size)
            return (m_data[m_size - 1]);
        else
            throw (std::out_of_range("back"));
    }

    void        pop_back()
    {
        if (m_data && m_size)
            --m_size;
        else
            throw (std::out_of_range("pop_back"));
    }

    T&          at(std::size_t pos)
    {
        if (m_data && pos < m_size)
            return (m_data[pos]);
        else
            throw (std::out_of_range("at"));
    }

    const T&    at(std::size_t pos) const
    {
        if (m_data && pos < m_size)
            return (m_data[pos]);
        else
            throw (std::out_of_range("at"));
    }

    T&          operator[](std::size_t pos)
    {
        return (at(pos));
    }

    const T&    operator[](std::size_t pos) const
    {
        return (at(pos));
    }

    void        resize(std::size_t new_size)
    {
        const T value = T();
        resize(new_size, value);
    }

    void        resize(std::size_t new_size, const T& value)
    {
        reserve(new_size);

        for (size_t i = m_size; i < new_size; ++i)
            m_data[i] = value;

        m_size = new_size;
   }

    void        reserve(std::size_t new_capacity)
    {
        if (new_capacity > m_capacity)
        {
            T   *new_data = new T[new_capacity];

            std::copy(begin(), end(), new_data);
            delete[] m_data;

            m_capacity = new_capacity;
            m_data = new_data;
        }
    }

    std::size_t size() const noexcept
    {
        return (m_size);
    }

    bool        empty() const noexcept
    {
        return (m_size == 0);
    }

    T*          begin() noexcept
    {
        return (m_data);
    }

    const T*    begin() const noexcept
    {
        return (m_data);
    }

    T*          end() noexcept
    {
        return (m_data + m_size);
    }

    const T*    end() const noexcept
    {
        return (m_data + m_size);
    }

    T*          data() noexcept
    {
        return (m_data);
    }

    const T*    data() const noexcept
    {
        return (m_data);
    }

    void        clear() noexcept
    {
        m_size = 0;
    }

    void        shrink_to_fit()
    {
        if (m_size != m_capacity)
        {
            T   *new_data = new T[m_size];
            std::copy(begin(), end(), new_data);
            delete[] m_data;

            m_data = new_data;
            m_capacity = m_size;
        }
    }

    void        reverse()
    {
        if (!m_size)
            return ;

        size_t i = 0;
        size_t j = m_size - 1;

        while (i < j)
        {
            std::swap(m_data[i], m_data[j]);
            ++i;
            --j;
        }
    }

    virtual
    ~FtVector()
    {
        delete[] m_data;
        m_data = nullptr;
    }

    void        unsafe_resize(std::size_t new_size)
    {
        reserve(new_size);
        m_size = new_size;
    }

  private:

    std::size_t         m_size;
    std::size_t         m_capacity;
    T                   *m_data;
    const std::size_t   mc_capacity_increase_rate = 2;
};

#endif /* FT_VECTOR_H */
#ifndef ORDER_STATISTICS_H
# define ORDER_STATISTICS_H

# include <cstddef>
# include <algorithm>



template <typename T = int, typename Comparator = DefaultComparator<T>>
ssize_t     median_of_3(FtVector<T>& vector,
                    size_t left, size_t right,
                    Comparator cmp = Comparator())
{
    if (left >= vector.size()
        || right > vector.size())
        return (-1);

    size_t mid = (left + right) / 2;
    --right;

    if (cmp(vector[mid], vector[right]))
    {
        if (cmp(vector[left], vector[mid]))
            return (mid);
        else
            return (cmp(vector[left], vector[right]) ? left : right);
    }
    else
    {
        if (cmp(vector[left], vector[mid]))
            return (cmp(vector[right], vector[left]) ? left : right);
        else
            return (mid);
    }
}

template <typename T = int, typename Comparator = DefaultComparator<T>>
ssize_t      qsort_partition_forward(FtVector<T>& vector,
                                    size_t left, size_t right, size_t pivot,
                                    Comparator cmp = Comparator())
{
    if (pivot >= vector.size()
        || left >= vector.size()
        || right > vector.size())
        return (-1);

    size_t  i = left;

    std::swap(vector[pivot], vector[right - 1]);
    for (size_t j = left; j < right - 1; ++j)
    {
        if (!cmp(vector[right - 1], vector[j]))
        {
            std::swap(vector[i], vector[j]);
            ++i;
        }
    }
    std::swap(vector[i], vector[right - 1]);

    return (i);
}

template <typename T = int, typename Comparator = DefaultComparator<T>>
ssize_t      qsort_partition_backward(FtVector<T>& vector,
                                        size_t left, size_t right, size_t pivot,
                                        Comparator cmp = Comparator())
{
    if (pivot >= vector.size()
        || left >= vector.size()
        || right > vector.size())
        return (-1);

    size_t  i = right - 1;

    std::swap(vector[pivot], vector[left]);
    for (size_t j = right - 1; j > left; --j)
    {
        if (cmp(vector[left], vector[j]))
        {
            std::swap(vector[i], vector[j]);
            --i;
        }
    }
    std::swap(vector[i], vector[left]);

    return (i);
}

template <typename T, typename Comparator = DefaultComparator<T>>
ssize_t     order_statistic(FtVector<T>& vector, size_t k,
                            Comparator cmp = Comparator())
{
    ssize_t mid;
    size_t  left = 0;
    size_t  right = vector.size();
    while (true)
    {
        size_t  pivot = median_of_3(vector, left, right, cmp);
        mid = qsort_partition_backward(vector, left, right, pivot, cmp);
        if (static_cast<size_t>(mid) == k || mid == -1)
            break ;

        if (k < static_cast<size_t>(mid))
            right = mid;
        else
            left = mid + 1;
    }
    return (mid);
}

#endif /* ORDER_STATISTICS_H */
#include <cstddef>
#include <iostream>


int             main(void)
{
    size_t n, k;

    std::cin >> n >> k;
    auto vector = FtVector<int>(n);

    for (size_t i = 0; i < n; ++i)
    {
        int tmp;
        std::cin >> tmp;
        vector.push_back(tmp);
    }

    std::cout << vector[order_statistic(vector, k)] << std::endl;
    return (0);
}
