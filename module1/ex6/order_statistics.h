#ifndef ORDER_STATISTICS_H
# define ORDER_STATISTICS_H

# include <cstddef>
# include <algorithm>

# include "ft_vector.h"
# include "default_comparator.h"


template <typename T = int, typename Comparator = DefaultComparator<T>>
ssize_t     median_of_3(FtVector<T>& vector,
                    size_t left, size_t right,
                    Comparator cmp = Comparator())
{
    if (left >= vector.size()
        || right > vector.size())
        return (-1);

    size_t mid = (left + right) / 2;
    --right;

    if (cmp(vector[mid], vector[right]))
    {
        if (cmp(vector[left], vector[mid]))
            return (mid);
        else
            return (cmp(vector[left], vector[right]) ? left : right);
    }
    else
    {
        if (cmp(vector[left], vector[mid]))
            return (cmp(vector[right], vector[left]) ? left : right);
        else
            return (mid);
    }
}

template <typename T = int, typename Comparator = DefaultComparator<T>>
ssize_t      qsort_partition_forward(FtVector<T>& vector,
                                    size_t left, size_t right, size_t pivot,
                                    Comparator cmp = Comparator())
{
    if (pivot >= vector.size()
        || left >= vector.size()
        || right > vector.size())
        return (-1);

    size_t  i = left;

    std::swap(vector[pivot], vector[right - 1]);
    for (size_t j = left; j < right - 1; ++j)
    {
        if (!cmp(vector[right - 1], vector[j]))
        {
            std::swap(vector[i], vector[j]);
            ++i;
        }
    }
    std::swap(vector[i], vector[right - 1]);

    return (i);
}

template <typename T = int, typename Comparator = DefaultComparator<T>>
ssize_t      qsort_partition_backward(FtVector<T>& vector,
                                        size_t left, size_t right, size_t pivot,
                                        Comparator cmp = Comparator())
{
    if (pivot >= vector.size()
        || left >= vector.size()
        || right > vector.size())
        return (-1);

    size_t  i = right - 1;

    std::swap(vector[pivot], vector[left]);
    for (size_t j = right - 1; j > left; --j)
    {
        if (cmp(vector[left], vector[j]))
        {
            std::swap(vector[i], vector[j]);
            --i;
        }
    }
    std::swap(vector[i], vector[left]);

    return (i);
}

template <typename T, typename Comparator = DefaultComparator<T>>
ssize_t     order_statistic(FtVector<T>& vector, size_t k,
                            Comparator cmp = Comparator())
{
    ssize_t mid;
    size_t  left = 0;
    size_t  right = vector.size();
    while (true)
    {
        size_t  pivot = median_of_3(vector, left, right, cmp);
        mid = qsort_partition_backward(vector, left, right, pivot, cmp);
        if (static_cast<size_t>(mid) == k || mid == -1)
            break ;

        if (k < static_cast<size_t>(mid))
            right = mid;
        else
            left = mid + 1;
    }
    return (mid);
}

#endif /* ORDER_STATISTICS_H */
