#include <iostream>

#include "stack_based_queue.h"

int     main(void)
{
    size_t                  commands_count;
    StackBasedQueue<int>    queue;

    std::cin >> commands_count;
    for (size_t i = 0; i < commands_count; ++i)
    {
        int command, value;

        std::cin >> command >> value;

        switch (command)
        {
            case 3:
            {
                queue.push_back(value);
                break ;
            }
            case 2:
            {
                try
                {
                    auto front = queue.pop_front();
                    if (front != value)
                    {
                        std::cout << "NO" << std::endl;
                        return (0);
                    }
                }
                catch (std::out_of_range&)
                {
                    if (value != -1)
                    {
                        std::cout << "NO" << std::endl;
                        return (0);
                    }
                }
                break ;
            }
            default:
                return (1);
        }
    }
    std::cout << "YES" << std::endl;
    return (0);
}
