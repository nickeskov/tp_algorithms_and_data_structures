#ifndef STACK_BASED_QUEUE_H
#define STACK_BASED_QUEUE_H

#include <utility>

#include "ft_vector.h"

template <typename T>
class StackBasedQueue
{
  public:

    StackBasedQueue() = default;

    StackBasedQueue(size_t n): m_push_stack(n), m_pop_stack(n) {}

    StackBasedQueue(const StackBasedQueue& other)
        : m_push_stack(other.m_push_stack)
        , m_pop_stack(other.m_pop_stack) {}

    StackBasedQueue(StackBasedQueue&& other)
        : m_push_stack(std::move(other.m_push_stack))
        , m_pop_stack(std::move(other.m_pop_stack)) {}

    StackBasedQueue& operator=(const StackBasedQueue& other)
    {
        if (this == &other)
            return (*this);
        m_push_stack = other.m_push_stack;
        m_pop_stack = other.m_pop_stack;

        return (*this);
    }

    StackBasedQueue& operator=(StackBasedQueue&& other)
    {
        if (this == &other)
            return (*this);
        m_push_stack = std::move(other.m_push_stack);
        m_pop_stack = std::move(other.m_pop_stack);

        return (*this);
    }

    size_t  size()
    {
        return (m_push_stack.size() + m_pop_stack.size());
    }

    bool    empty()
    {
        return (m_push_stack.empty() && m_pop_stack.empty());
    }

    void    push_back(const T& elem)
    {
        m_push_stack.push_back(elem);
    }

    void    push_back(T&& elem)
    {
        m_push_stack.push_back(std::move(elem));
    }

    T    pop_front()
    {
        if (m_pop_stack.empty() && !m_push_stack.empty())
        {
            m_push_stack.reverse();
            m_pop_stack = std::move(m_push_stack);
            m_push_stack = FtVector<T>(m_pop_stack.size());
        }
        T front = m_pop_stack.back();
        m_pop_stack.pop_back();
        return (front);
    }

  private:
    FtVector<T>     m_push_stack;
    FtVector<T>     m_pop_stack;
};

#endif /* STACK_BASED_QUEUE_H */
