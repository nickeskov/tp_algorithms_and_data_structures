#include <iostream>
#include <stdexcept>

#include "ft_vector.h"

template <typename T>
static inline void          init_vector(FtVector<T>& vect, std::size_t n)
{
    T tmp;

    for (size_t i = 0; i < n; ++i)
    {
        std::cin >> tmp;
        vect.push_back(tmp);
    }
}

template <typename T>
static inline std::size_t   find_pairs(FtVector<T>& v1, FtVector<T>& v2, T& k)
{
    std::size_t pairs_num = 0;

    if (v1.size() && v2.size())
    {
        long i = 0;
        long j = v2.size() - 1;

        while (i != v1.size() && j != -1)
        {
            T pair_sum = v1[i] + v2[j];

            if (pair_sum == k)
            {
                ++pairs_num;
                ++i;
                --j;
            }
            else if (pair_sum > k)
            {
                --j;
            }
            else
            {
                ++i;
            }
        }
    }
    return (pairs_num);
}

int                         main(void)
{
    std::size_t n, m;
    long        k;

    std::cin >> n;
    auto a_vect = FtVector<long>(n);
    init_vector(a_vect, n);

    std::cin >> m;
    auto b_vect = FtVector<long>(m);
    init_vector(b_vect, m);

    std::cin >> k;

    std::cout << find_pairs(a_vect, b_vect, k) << std::endl;

    return (0);
}
