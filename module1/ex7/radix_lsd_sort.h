#ifndef RADIX_LSD_SORT_H
# define RADIX_LSD_SORT_H

# include <cstddef>
# include <utility>
# include <limits>
# include <algorithm>

# include "ft_vector.h"

typedef unsigned char       uchar;

template <typename T = unsigned long long>
static uchar get_byte(T& num, size_t pos)
{
    const uchar res = *(((uchar *) &num) + pos);
    return (res);
}

template <typename T = unsigned long long>
void    radix_lsd_sort(FtVector<T>& vector)
{
    FtVector<size_t>    bytes_count(std::numeric_limits<uchar>::max() + 1);

    FtVector<T>         partially_sorted(vector.size());
    partially_sorted.unsafe_resize(vector.size());

    for (size_t i = 0; i < sizeof(T); ++i)
    {
        bytes_count.clear();
        bytes_count.resize(std::numeric_limits<uchar>::max() + 1);

        for (auto & num : vector)
        {
            const auto byte = get_byte(num, i);
            ++(bytes_count[byte]);
        }

        // convert to element positions
        size_t count = 0;
        for (auto & num : bytes_count)
        {
            const auto tmp = num;
            num = count;
            count += tmp;
        }

        for (auto & num : vector)
        {
            const auto byte = get_byte(num, i);
            partially_sorted[bytes_count[byte]] = num;
            ++(bytes_count[byte]);
        }

        std::swap(vector, partially_sorted);
    }
}

#endif /* RADIX_LSD_SORT_H */
