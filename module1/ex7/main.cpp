#ifndef FT_VECTOR_H
# define FT_VECTOR_H

#include <algorithm>
#include <utility>
#include <stdexcept>

template <typename T>
class FtVector
{
  public:

    FtVector(): m_size(0), m_capacity(0), m_data(nullptr) {}

    explicit FtVector(std::size_t init_size)
        : m_size(0)
        , m_capacity(init_size)
        , m_data(init_size ? new T[init_size] : nullptr) {}

    FtVector(const FtVector& other)
        : m_size(other.m_size)
        , m_capacity(other.m_capacity)
        , m_data(other.m_capacity ? new T[other.m_capacity] : nullptr)

    {
        std::copy(other.begin(), other.end(), begin());
    }

    FtVector(FtVector&& other) noexcept
        : m_size(other.m_size)
        , m_capacity(other.m_capacity)
        , m_data(other.m_data)
    {
        other.m_size = 0;
        other.m_capacity = 0;
        other.m_data = nullptr;
    }

    FtVector&   operator=(const FtVector& other)
    {
        if (this == &other)
            return (*this);

        delete[] m_data;
        m_data = new T[other.m_capacity];
        m_capacity = other.m_capacity;
        m_size = other.m_size;
        std::copy(other.begin(), other.end(), begin());
        return (*this);
    }

    FtVector&   operator=(FtVector&& other) noexcept
    {
        if (this == &other)
            return (*this);

        delete[] m_data;
        m_size = other.m_size;
        m_capacity = other.m_capacity;
        m_data = other.m_data;

        other.m_size = 0;
        other.m_capacity = 0;
        other.m_data = nullptr;

        return (*this);
    }

    void        push_back(const T& elem)
    {
        if (m_size == m_capacity)
        {
            std::size_t new_capacity;
            new_capacity = (m_size ?
                m_size * mc_capacity_increase_rate :
                mc_capacity_increase_rate);
            reserve(new_capacity);
        }
        m_data[m_size] = elem;
        ++m_size;
    }

    void        push_back(T&& elem)
    {
        if (m_size == m_capacity)
        {
            std::size_t new_capacity;
            new_capacity = (m_size ?
                m_size * mc_capacity_increase_rate :
                mc_capacity_increase_rate);
            reserve(new_capacity);
        }
        m_data[m_size] = std::move(elem);
        ++m_size;
    }

    T&          front()
    {
        if (m_data && m_size)
            return (*m_data);
        else
            throw (std::out_of_range("front"));
    }

    const T&    front() const
    {
        if (m_data && m_size)
            return (*m_data);
        else
            throw (std::out_of_range("front"));
    }

    T&          back()
    {
        if (m_data && m_size)
            return (m_data[m_size - 1]);
        else
            throw (std::out_of_range("back"));
    }

    const T&    back() const
    {
        if (m_data && m_size)
            return (m_data[m_size - 1]);
        else
            throw (std::out_of_range("back"));
    }

    void        pop_back()
    {
        if (m_data && m_size)
            --m_size;
        else
            throw (std::out_of_range("pop_back"));
    }

    T&          at(std::size_t pos)
    {
        if (m_data && pos < m_size)
            return (m_data[pos]);
        else
            throw (std::out_of_range("at"));
    }

    const T&    at(std::size_t pos) const
    {
        if (m_data && pos < m_size)
            return (m_data[pos]);
        else
            throw (std::out_of_range("at"));
    }

    T&          operator[](std::size_t pos)
    {
        return (at(pos));
    }

    const T&    operator[](std::size_t pos) const
    {
        return (at(pos));
    }

    void        resize(std::size_t new_size)
    {
        const T value = T();
        resize(new_size, value);
    }

    void        resize(std::size_t new_size, const T& value)
    {
        reserve(new_size);

        for (size_t i = m_size; i < new_size; ++i)
            m_data[i] = value;

        m_size = new_size;
   }

    void        reserve(std::size_t new_capacity)
    {
        if (new_capacity > m_capacity)
        {
            T   *new_data = new T[new_capacity];

            std::copy(begin(), end(), new_data);
            delete[] m_data;

            m_capacity = new_capacity;
            m_data = new_data;
        }
    }

    std::size_t size() const noexcept
    {
        return (m_size);
    }

    bool        empty() const noexcept
    {
        return (m_size == 0);
    }

    T*          begin() noexcept
    {
        return (m_data);
    }

    const T*    begin() const noexcept
    {
        return (m_data);
    }

    T*          end() noexcept
    {
        return (m_data + m_size);
    }

    const T*    end() const noexcept
    {
        return (m_data + m_size);
    }

    T*          data() noexcept
    {
        return (m_data);
    }

    const T*    data() const noexcept
    {
        return (m_data);
    }

    void        clear() noexcept
    {
        m_size = 0;
    }

    void        shrink_to_fit()
    {
        if (m_size != m_capacity)
        {
            T   *new_data = new T[m_size];
            std::copy(begin(), end(), new_data);
            delete[] m_data;

            m_data = new_data;
            m_capacity = m_size;
        }
    }

    void        reverse()
    {
        if (!m_size)
            return ;

        size_t i = 0;
        size_t j = m_size - 1;

        while (i < j)
        {
            std::swap(m_data[i], m_data[j]);
            ++i;
            --j;
        }
    }

    virtual
    ~FtVector()
    {
        delete[] m_data;
        m_data = nullptr;
    }

    void        unsafe_resize(std::size_t new_size)
    {
        reserve(new_size);
        m_size = new_size;
    }

  private:

    std::size_t         m_size;
    std::size_t         m_capacity;
    T                   *m_data;
    const std::size_t   mc_capacity_increase_rate = 2;
};

#endif /* FT_VECTOR_H */
#ifndef RADIX_LSD_SORT_H
# define RADIX_LSD_SORT_H

# include <cstddef>
# include <utility>
# include <limits>
# include <algorithm>


typedef unsigned char       uchar;

template <typename T = unsigned long long>
static uchar get_byte(T& num, size_t pos)
{
    const uchar res = *(((uchar *) &num) + pos);
    return (res);
}

template <typename T = unsigned long long>
void    radix_lsd_sort(FtVector<T>& vector)
{
    FtVector<size_t>    bytes_count(std::numeric_limits<uchar>::max() + 1);

    FtVector<T>         partially_sorted(vector.size());
    partially_sorted.unsafe_resize(vector.size());

    for (size_t i = 0; i < sizeof(T); ++i)
    {
        bytes_count.clear();
        bytes_count.resize(std::numeric_limits<uchar>::max() + 1);

        for (auto & num : vector)
        {
            const auto byte = get_byte(num, i);
            ++(bytes_count[byte]);
        }

        // convert to element positions
        size_t count = 0;
        for (auto & num : bytes_count)
        {
            const auto tmp = num;
            num = count;
            count += tmp;
        }

        for (auto & num : vector)
        {
            const auto byte = get_byte(num, i);
            partially_sorted[bytes_count[byte]] = num;
            ++(bytes_count[byte]);
        }

        std::swap(vector, partially_sorted);
    }
}

#endif /* RADIX_LSD_SORT_H */
#include <iostream>


int     main(void)
{
    size_t n;

    std::cin >> n;

    FtVector<unsigned long long> vector(n);
    vector.unsafe_resize(n);

    for (auto & num : vector)
        std::cin >> num;

    radix_lsd_sort(vector);

    for (const auto & num : vector)
        std::cout << num << std::endl;

    return (0);
}
