#include <iostream>

#include "ft_vector.h"
#include "radix_lsd_sort.h"

int     main(void)
{
    size_t n;

    std::cin >> n;

    FtVector<unsigned long long> vector(n);
    vector.unsafe_resize(n);

    for (auto & num : vector)
        std::cin >> num;

    radix_lsd_sort(vector);

    for (const auto & num : vector)
        std::cout << num << std::endl;

    return (0);
}
