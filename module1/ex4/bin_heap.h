#ifndef BIN_HEAP_H
# define BIN_HEAP_H

# include <utility>
# include <algorithm>

# include "default_comparator.h"
# include "ft_vector.h"

# define LEFT_CHILD(i) ((2 * (i) + 1))
# define RIGHT_CHILD(i) ((2 * (i) + 2))
# define PARENT(i) (((i) - 1) / 2)

template <typename T, typename Comparator = DefaultComparator<T>>
class Heap
{
  public:

    Heap() = default;

    explicit Heap(const FtVector<T>& vect): m_vect(vect)
    {
        build_heap();
    }

    explicit Heap(FtVector<T>&& vect): m_vect(std::forward<FtVector<T>>(vect))
    {
        build_heap();
    }

    Heap(const Heap& other)
        : m_vect(other.m_vect)
        , m_cmp(other.m_cmp) {}

    Heap(Heap&& other) noexcept
        : m_vect(std::move(other.m_vect))
        , m_cmp(std::move(other.m_cmp)) {}

    Heap&               operator=(const Heap& other)
    {
        if (this == &other)
            return (*this);
        m_vect = other.m_vect;
        m_cmp = other.m_cmp;
    }

    Heap&               operator=(Heap&& other) noexcept
    {
        if (this == &other)
            return (*this);
        m_vect = std::move(other.m_vect);
        m_cmp = std::move(other.m_cmp);
    }

    void                insert(const T& elem);

    void                insert(T&& elem);

    T                   extract_top();

    const T&            top() const;

    size_t              size() const noexcept;

    bool                empty() const noexcept;

    FtVector<T>&        content() noexcept;

    const FtVector<T>&  content() const noexcept;

    static void         sort(FtVector<T>& vect);

  private:

    FtVector<T> m_vect;
    Comparator  m_cmp;

    void                build_heap();
    void                shift_down(size_t i);
    void                shift_up(size_t i);
};

template <typename T, typename Comparator>
void        Heap<T, Comparator>::insert(const T& elem)
{
    m_vect.push_back(elem);
    shift_up(size() - 1);
}

template <typename T, typename Comparator>
void        Heap<T, Comparator>::insert(T&& elem)
{
    m_vect.push_back(std::forward<T>(elem));
    shift_up(size() - 1);
}

template <typename T, typename Comparator>
T           Heap<T, Comparator>::extract_top()
{
    T result = std::move(m_vect.front());

    m_vect[0] = std::move(m_vect.back());
    m_vect.pop_back();

    if (!m_vect.empty())
        shift_down(0);
    return (result);
}

template <typename T, typename Comparator>
size_t      Heap<T, Comparator>::size() const noexcept
{
    return (m_vect.size());
}

template <typename T, typename Comparator>
bool                Heap<T, Comparator>::empty() const noexcept
{
    return (m_vect.empty());
}

template <typename T, typename Comparator>
const T&            Heap<T, Comparator>::top() const
{
    return (m_vect.front());
}

template <typename T, typename Comparator>
FtVector<T>&        Heap<T, Comparator>::content() noexcept
{
    return (m_vect);
}

template <typename T, typename Comparator>
const FtVector<T>&  Heap<T, Comparator>::content() const noexcept
{
    return (m_vect);
}



template <typename T , typename Comparator>
void                 Heap<T, Comparator>::sort(FtVector<T>& vect)
{
    const auto  vect_size = vect.size();
    auto        heap = Heap<T, Comparator>(std::move(vect));

    while (heap.size() > 1)
    {
        std::swap(heap.content().front(), heap.content().back());
        heap.content().pop_back();
        heap.shift_down(0);
    }
    heap.content().unsafe_resize(vect_size);
    vect = std::move(heap.content());
}



template <typename T, typename Comparator>
void                Heap<T, Comparator>::build_heap()
{
    for (ssize_t i = size() / 2 - 1; i >= 0; --i)
        shift_down(i);
}

template <typename T, typename Comparator>
void                Heap<T, Comparator>::shift_down(size_t i)
{
    bool    do_shift_down = true;

    while (do_shift_down)
    {
        size_t  left = LEFT_CHILD(i);
        size_t  right = RIGHT_CHILD(i);

        size_t  largest = i;
        if (left < size() && m_cmp(m_vect[i], m_vect[left]))
            largest = left;
        if (right < size() && m_cmp(m_vect[largest], m_vect[right]))
            largest = right;

        if (largest != i)
        {
            std::swap(m_vect[i], m_vect[largest]);
            i = largest;
        }
        else
            do_shift_down = false;
    }
}

template <typename T, typename Comparator>
void                Heap<T, Comparator>::shift_up(size_t i)
{
    while (i > 0)
    {
        size_t parent = PARENT(i);
        if (!m_cmp(m_vect[parent], m_vect[i]))
            break ;
        std::swap(m_vect[i], m_vect[parent]);
        i = parent;
    }
}

# undef LEFT_CHILD
# undef RIGHT_CHILD
# undef PARENT

#endif /* BIN_HEAP_H */
