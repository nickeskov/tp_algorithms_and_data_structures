#ifndef DEFAULT_COMPARATOR_H
#define DEFAULT_COMPARATOR_H

template <typename T>
class DefaultComparator
{
  public:
    bool operator()(const T& lhs, const T& rhs)
    {
        return (lhs < rhs);
    }
};

#endif /* DEFAULT_COMPARATOR_H */
