#include <iostream>
#include <utility>

#include "default_comparator.h"
#include "ft_vector.h"
#include "bin_heap.h"

template <typename T, typename Comparator = DefaultComparator<T>>
static size_t   greedy(Heap<T, Comparator>& heap, size_t k)
{
    size_t      count = 0;
    FtVector<T> cap;

    if (!heap.empty() && heap.top() <= k)
    {
        while (!heap.empty())
        {
            size_t weight_cap = k;
            while (weight_cap && !heap.empty() && weight_cap >= heap.top())
            {
                T curr_top = heap.extract_top();
                weight_cap -= curr_top;
                if (curr_top != 1)
                    cap.push_back(curr_top);
            }
            for (size_t i = 0; i < cap.size(); ++i)
                heap.insert(cap[i] / 2);
            cap.clear();
            ++count;
        }

    }
    return (count);
}

int             main(void)
{
    size_t n, k;

    std::cin >> n;
    auto vect = FtVector<size_t>(n);

    for (size_t i = 0; i< n; ++i)
    {
        size_t tmp;
        std::cin >> tmp;
        vect.push_back(tmp);
    }
    auto heap = Heap<size_t>(std::move(vect));

    std::cin >> k;
    std::cout << greedy(heap, k) << std::endl;
    return (0);
}
