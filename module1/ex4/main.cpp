#ifndef DEFAULT_COMPARATOR_H
#define DEFAULT_COMPARATOR_H

template <typename T>
class DefaultComparator
{
  public:
    bool operator()(const T& lhs, const T& rhs)
    {
        return (lhs < rhs);
    }
};

#endif /* DEFAULT_COMPARATOR_H */
#ifndef FT_VECTOR_H
# define FT_VECTOR_H

#include <algorithm>
#include <utility>
#include <stdexcept>

template <typename T>
class FtVector
{
  public:

    FtVector(): m_size(0), m_capacity(0), m_data(nullptr) {}

    explicit FtVector(std::size_t init_size)
        : m_size(0)
        , m_capacity(init_size)
        , m_data(init_size ? new T[init_size] : nullptr) {}

    FtVector(const FtVector& other)
        : m_size(other.m_size)
        , m_capacity(other.m_capacity)
        , m_data(other.m_capacity ? new T[other.m_capacity] : nullptr)

    {
        std::copy(other.begin(), other.end(), begin());
    }

    FtVector(FtVector&& other) noexcept
        : m_size(other.m_size)
        , m_capacity(other.m_capacity)
        , m_data(other.m_data)
    {
        other.m_size = 0;
        other.m_capacity = 0;
        other.m_data = nullptr;
    }

    FtVector&   operator=(const FtVector& other)
    {
        if (this == &other)
            return (*this);

        delete[] m_data;
        m_data = new T[other.m_capacity];
        m_capacity = other.m_capacity;
        m_size = other.m_size;
        std::copy(other.begin(), other.end(), begin());
        return (*this);
    }

    FtVector&   operator=(FtVector&& other) noexcept
    {
        if (this == &other)
            return (*this);

        delete[] m_data;
        m_size = other.m_size;
        m_capacity = other.m_capacity;
        m_data = other.m_data;

        other.m_size = 0;
        other.m_capacity = 0;
        other.m_data = nullptr;

        return (*this);
    }

    void        push_back(const T& elem)
    {
        if (m_size == m_capacity)
        {
            std::size_t new_capacity;
            new_capacity = (m_size ?
                m_size * mc_capacity_increase_rate :
                mc_capacity_increase_rate);
            reserve(new_capacity);
        }
        m_data[m_size] = elem;
        ++m_size;
    }

    void        push_back(T&& elem)
    {
        if (m_size == m_capacity)
        {
            std::size_t new_capacity;
            new_capacity = (m_size ?
                m_size * mc_capacity_increase_rate :
                mc_capacity_increase_rate);
            reserve(new_capacity);
        }
        m_data[m_size] = std::move(elem);
        ++m_size;
    }

    T&          front()
    {
        if (m_data && m_size)
            return (*m_data);
        else
            throw (std::out_of_range("front"));
    }

    const T&    front() const
    {
        if (m_data && m_size)
            return (*m_data);
        else
            throw (std::out_of_range("front"));
    }

    T&          back()
    {
        if (m_data && m_size)
            return (m_data[m_size - 1]);
        else
            throw (std::out_of_range("back"));
    }

    const T&    back() const
    {
        if (m_data && m_size)
            return (m_data[m_size - 1]);
        else
            throw (std::out_of_range("back"));
    }

    void        pop_back()
    {
        if (m_data && m_size)
            --m_size;
        else
            throw (std::out_of_range("pop_back"));
    }

    T&          at(std::size_t pos)
    {
        if (m_data && pos < m_size)
            return (m_data[pos]);
        else
            throw (std::out_of_range("at"));
    }

    const T&    at(std::size_t pos) const
    {
        if (m_data && pos < m_size)
            return (m_data[pos]);
        else
            throw (std::out_of_range("at"));
    }

    T&          operator[](std::size_t pos)
    {
        return (at(pos));
    }

    const T&    operator[](std::size_t pos) const
    {
        return (at(pos));
    }

    void        resize(std::size_t new_size)
    {
        const T value = T();
        resize(new_size, value);
    }

    void        resize(std::size_t new_size, const T& value)
    {
        reserve(new_size);

        for (size_t i = m_size; i < new_size; ++i)
            m_data[i] = value;

        m_size = new_size;
   }

    void        reserve(std::size_t new_capacity)
    {
        if (new_capacity > m_capacity)
        {
            T   *new_data = new T[new_capacity];

            std::copy(begin(), end(), new_data);
            delete[] m_data;

            m_capacity = new_capacity;
            m_data = new_data;
        }
    }

    std::size_t size() const noexcept
    {
        return (m_size);
    }

    bool        empty() const noexcept
    {
        return (m_size == 0);
    }

    T*          begin() noexcept
    {
        return (m_data);
    }

    const T*    begin() const noexcept
    {
        return (m_data);
    }

    T*          end() noexcept
    {
        return (m_data + m_size);
    }

    const T*    end() const noexcept
    {
        return (m_data + m_size);
    }

    T*          data() noexcept
    {
        return (m_data);
    }

    const T*    data() const noexcept
    {
        return (m_data);
    }

    void        clear() noexcept
    {
        m_size = 0;
    }

    void        shrink_to_fit()
    {
        if (m_size != m_capacity)
        {
            T   *new_data = new T[m_size];
            std::copy(begin(), end(), new_data);
            delete[] m_data;

            m_data = new_data;
            m_capacity = m_size;
        }
    }

    void        reverse()
    {
        if (!m_size)
            return ;

        size_t i = 0;
        size_t j = m_size - 1;

        while (i < j)
        {
            std::swap(m_data[i], m_data[j]);
            ++i;
            --j;
        }
    }

    virtual
    ~FtVector()
    {
        delete[] m_data;
        m_data = nullptr;
    }

    void        unsafe_resize(std::size_t new_size)
    {
        reserve(new_size);
        m_size = new_size;
    }

  private:

    std::size_t         m_size;
    std::size_t         m_capacity;
    T                   *m_data;
    const std::size_t   mc_capacity_increase_rate = 2;
};

#endif /* FT_VECTOR_H */
#ifndef BIN_HEAP_H
# define BIN_HEAP_H

# include <utility>
# include <algorithm>


# define LEFT_CHILD(i) ((2 * (i) + 1))
# define RIGHT_CHILD(i) ((2 * (i) + 2))
# define PARENT(i) (((i) - 1) / 2)

template <typename T, typename Comparator = DefaultComparator<T>>
class Heap
{
  public:

    Heap() = default;

    explicit Heap(const FtVector<T>& vect): m_vect(vect)
    {
        build_heap();
    }

    explicit Heap(FtVector<T>&& vect): m_vect(std::forward<FtVector<T>>(vect))
    {
        build_heap();
    }

    Heap(const Heap& other)
        : m_vect(other.m_vect)
        , m_cmp(other.m_cmp) {}

    Heap(Heap&& other) noexcept
        : m_vect(std::move(other.m_vect))
        , m_cmp(std::move(other.m_cmp)) {}

    Heap&               operator=(const Heap& other)
    {
        if (this == &other)
            return (*this);
        m_vect = other.m_vect;
        m_cmp = other.m_cmp;
    }

    Heap&               operator=(Heap&& other) noexcept
    {
        if (this == &other)
            return (*this);
        m_vect = std::move(other.m_vect);
        m_cmp = std::move(other.m_cmp);
    }

    void                insert(const T& elem);

    void                insert(T&& elem);

    T                   extract_top();

    const T&            top() const;

    size_t              size() const noexcept;

    bool                empty() const noexcept;

    FtVector<T>&        content() noexcept;

    const FtVector<T>&  content() const noexcept;

    static void         sort(FtVector<T>& vect);

  private:

    FtVector<T> m_vect;
    Comparator  m_cmp;

    void                build_heap();
    void                shift_down(size_t i);
    void                shift_up(size_t i);
};

template <typename T, typename Comparator>
void        Heap<T, Comparator>::insert(const T& elem)
{
    m_vect.push_back(elem);
    shift_up(size() - 1);
}

template <typename T, typename Comparator>
void        Heap<T, Comparator>::insert(T&& elem)
{
    m_vect.push_back(std::forward<T>(elem));
    shift_up(size() - 1);
}

template <typename T, typename Comparator>
T           Heap<T, Comparator>::extract_top()
{
    T result = std::move(m_vect.front());

    m_vect[0] = std::move(m_vect.back());
    m_vect.pop_back();

    if (!m_vect.empty())
        shift_down(0);
    return (result);
}

template <typename T, typename Comparator>
size_t      Heap<T, Comparator>::size() const noexcept
{
    return (m_vect.size());
}

template <typename T, typename Comparator>
bool                Heap<T, Comparator>::empty() const noexcept
{
    return (m_vect.empty());
}

template <typename T, typename Comparator>
const T&            Heap<T, Comparator>::top() const
{
    return (m_vect.front());
}

template <typename T, typename Comparator>
FtVector<T>&        Heap<T, Comparator>::content() noexcept
{
    return (m_vect);
}

template <typename T, typename Comparator>
const FtVector<T>&  Heap<T, Comparator>::content() const noexcept
{
    return (m_vect);
}



template <typename T , typename Comparator>
void                 Heap<T, Comparator>::sort(FtVector<T>& vect)
{
    const auto  vect_size = vect.size();
    auto        heap = Heap<T, Comparator>(std::move(vect));

    while (heap.size() > 1)
    {
        std::swap(heap.content().front(), heap.content().back());
        heap.content().pop_back();
        heap.shift_down(0);
    }
    heap.content().unsafe_resize(vect_size);
    vect = std::move(heap.content());
}



template <typename T, typename Comparator>
void                Heap<T, Comparator>::build_heap()
{
    for (ssize_t i = size() / 2 - 1; i >= 0; --i)
        shift_down(i);
}

template <typename T, typename Comparator>
void                Heap<T, Comparator>::shift_down(size_t i)
{
    bool    do_shift_down = true;

    while (do_shift_down)
    {
        size_t  left = LEFT_CHILD(i);
        size_t  right = RIGHT_CHILD(i);

        size_t  largest = i;
        if (left < size() && m_cmp(m_vect[i], m_vect[left]))
            largest = left;
        if (right < size() && m_cmp(m_vect[largest], m_vect[right]))
            largest = right;

        if (largest != i)
        {
            std::swap(m_vect[i], m_vect[largest]);
            i = largest;
        }
        else
            do_shift_down = false;
    }
}

template <typename T, typename Comparator>
void                Heap<T, Comparator>::shift_up(size_t i)
{
    while (i > 0)
    {
        size_t parent = PARENT(i);
        if (!m_cmp(m_vect[parent], m_vect[i]))
            break ;
        std::swap(m_vect[i], m_vect[parent]);
        i = parent;
    }
}

# undef LEFT_CHILD
# undef RIGHT_CHILD
# undef PARENT

#endif /* BIN_HEAP_H */
#include <iostream>
#include <utility>


template <typename T, typename Comparator = DefaultComparator<T>>
static size_t   greedy(Heap<T, Comparator>& heap, size_t k)
{
    size_t      count = 0;
    FtVector<T> cap;

    if (!heap.empty() && heap.top() <= k)
    {
        while (!heap.empty())
        {
            size_t weight_cap = k;
            while (weight_cap && !heap.empty() && weight_cap >= heap.top())
            {
                T curr_top = heap.extract_top();
                weight_cap -= curr_top;
                if (curr_top != 1)
                    cap.push_back(curr_top);
            }
            for (size_t i = 0; i < cap.size(); ++i)
                heap.insert(cap[i] / 2);
            cap.clear();
            ++count;
        }

    }
    return (count);
}

int             main(void)
{
    size_t n, k;

    std::cin >> n;
    auto vect = FtVector<size_t>(n);

    for (size_t i = 0; i< n; ++i)
    {
        size_t tmp;
        std::cin >> tmp;
        vect.push_back(tmp);
    }
    auto heap = Heap<size_t>(std::move(vect));

    std::cin >> k;
    std::cout << greedy(heap, k) << std::endl;
    return (0);
}
