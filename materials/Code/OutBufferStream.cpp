#include <iostream>
#include <vector>

using std::vector;

class OutBitStream {
public:
	OutBitStream();

	void WriteBit(unsigned char bit);
	void WriteByte(unsigned char byte);

	const unsigned char* GetBuffer() const;
	uint64_t GetBitsCount() const;

private:
	vector<unsigned char> buffer;
	uint64_t bitsCount;
};

OutBitStream::OutBitStream() : bitsCount(0)
{
}

void OutBitStream::WriteBit(unsigned char bit)
{
	if (bitsCount + 1 > buffer.size() * 8)
		buffer.push_back(0);

	if (bit != 0)
		buffer[bitsCount / 8] |= 1 << (bitsCount % 8);

	++bitsCount;
}

void OutBitStream::WriteByte(unsigned char byte)
{
	if (bitsCount % 8 == 0) {
		buffer.push_back(byte);
	}
	else {
		int offset = bitsCount % 8;
		buffer[bitsCount / 8] |= byte << offset;
		buffer.push_back(byte >> (8 - offset));
	}

	bitsCount += 8;
}

const unsigned char* OutBitStream::GetBuffer() const
{
	return buffer.data();
}

uint64_t OutBitStream::GetBitsCount() const
{
	return bitsCount;
}

int main()
{
	unsigned char ch = 1;
	std::cout << (ch << 1);
	return 0;
}