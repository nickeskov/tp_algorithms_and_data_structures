#include <vector>
#include <iostream>
#include <queue>
#include <assert.h>

using std::vector;
using std::queue;
using std::cout;
using std::cin;

struct IGraph {
	virtual ~IGraph() {}
	
	// ���������� ����� �� from � to.
	virtual void AddEdge(int from, int to) = 0;

	virtual int VerticesCount() const  = 0;

	virtual std::vector<int> GetNextVertices(int vertex) const = 0;
	virtual std::vector<int> GetPrevVertices(int vertex) const = 0;
};

class  ListGraph : public IGraph {
public:
	ListGraph( int count );
	ListGraph( const IGraph& graph );
	virtual ~ListGraph();
	
	// ���������� ����� �� from � to.
	virtual void AddEdge(int from, int to) override;

	virtual int VerticesCount() const override;

	virtual vector<int> GetNextVertices(int vertex) const override;
	virtual vector<int> GetPrevVertices(int vertex) const override;

private:
	vector<vector<int>> adjacencyLists;
};

ListGraph::ListGraph( int count ) :
	adjacencyLists( count )
{
}

ListGraph::ListGraph( const IGraph& graph ) :
	ListGraph( graph.VerticesCount() )
{
	for( int i = 0; i < graph.VerticesCount(); i++ )
		adjacencyLists[i] = graph.GetNextVertices( i );
}

ListGraph::~ListGraph()
{
}
	
void ListGraph::AddEdge(int from, int to)
{
	assert( from >= 0 && from < adjacencyLists.size() );
	assert( to >= 0 && to < adjacencyLists.size() );
	adjacencyLists[from].push_back( to );
}

int ListGraph::VerticesCount() const
{
	return adjacencyLists.size();
}

vector<int> ListGraph::GetNextVertices(int vertex) const
{
	assert( vertex >= 0 && vertex < adjacencyLists.size() );
	return adjacencyLists[vertex];
}

vector<int> ListGraph::GetPrevVertices(int vertex) const
{
	assert( vertex >= 0 && vertex < adjacencyLists.size() );

	vector<int> result;
	for( int from = 0; from < adjacencyLists.size(); from++ )
		for( int i = 0; i < adjacencyLists[from].size(); i++ )
			if( adjacencyLists[from][i] == vertex )
				result.push_back( from );

	return result;
}

void BFS(const IGraph& graph, int vertex, void(*visit)(int) )
{
	vector<bool> visited( graph.VerticesCount(), false );
	std::queue<int> qu;
	qu.push( vertex );
	visited[vertex] = true;

	while( !qu.empty() ) {
		int current = qu.front();
		visit( current );
		qu.pop();
		vector<int> nextVertices = graph.GetNextVertices( current );
		for( int v : nextVertices )
			if( !visited[ v ] ) {
				qu.push( v );
				visited[v] = true;
 			}
	}
}

int main()
{
	ListGraph g(6);
	g.AddEdge(0, 5);
	g.AddEdge(0, 4);
	g.AddEdge(0, 2);
	g.AddEdge(1, 0);
	g.AddEdge(1, 2);
	g.AddEdge(2, 4);
	g.AddEdge(2, 3);
	g.AddEdge(3, 1);
	g.AddEdge(4, 3);
	g.AddEdge(5, 4);

	BFS( g, 5, [](int v) { cout << v << " "; } );
	return 0;
}
