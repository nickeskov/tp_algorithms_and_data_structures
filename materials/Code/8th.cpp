#include <array>
#include <iostream>
#include <queue>
#include <assert.h>
#include <string>
#include <unordered_map>

using std::array;
using std::queue;
using std::cout;
using std::cin;
using std::swap;
using std::ostream;
using std::endl;
using std::string;
using std::unordered_map;

const int SideSize = 3;
const int FieldSize = SideSize * SideSize;

const array<char, FieldSize> FinishState = {1, 2, 3, 4, 5, 6, 7, 8, 0};

class GameState {
public:
	GameState(const array<char, FieldSize>& _field);

	bool IsFinish() const;

	bool CanMoveLeft() const;
	bool CanMoveUp() const;
	bool CanMoveRight() const;
	bool CanMoveDown() const;

	GameState MoveLeft() const;
	GameState MoveUp() const;
	GameState MoveRight() const;
	GameState MoveDown() const;

	bool operator == (const GameState& state) const  { return state.field == field; }
	bool operator != (const GameState& state) const  { return !(*this == state); }

	friend ostream& operator << (ostream& out, const GameState& state);
	friend struct Hasher;

private:
	array<char, FieldSize> field;
	char zeroPos;
};

GameState::GameState(const array<char, FieldSize>& _field) :
	field(_field),
	zeroPos(-1)
{
	for( unsigned int i = 0; i < field.size(); i++ )
		if( field[i] == 0 )
			zeroPos = i;
	assert( zeroPos >= 0 );
}

bool GameState::IsFinish() const
{
	return field == FinishState;
}

bool GameState::CanMoveLeft() const
{
	return zeroPos % SideSize != 0;
}

bool GameState::CanMoveUp() const
{
	return zeroPos >= SideSize;
}

bool GameState::CanMoveRight() const
{
	return zeroPos % SideSize < SideSize - 1;
}

bool GameState::CanMoveDown() const
{
	return zeroPos < FieldSize - SideSize;
}

GameState GameState::MoveLeft() const
{
	assert( CanMoveLeft() );

	GameState newState(*this);
	swap( newState.field[zeroPos], newState.field[zeroPos - 1] );
	--newState.zeroPos;
	return newState;
}

GameState GameState::MoveUp() const
{
	assert( CanMoveUp() );

	GameState newState(*this);
	swap( newState.field[zeroPos], newState.field[zeroPos - SideSize] );
	newState.zeroPos -= SideSize;
	return newState;
}

GameState GameState::MoveRight() const
{
	assert( CanMoveRight() );

	GameState newState(*this);
	swap( newState.field[zeroPos], newState.field[zeroPos + 1] );
	++newState.zeroPos;
	return newState;
}

GameState GameState::MoveDown() const
{
	assert( CanMoveDown() );

	GameState newState(*this);
	swap( newState.field[zeroPos], newState.field[zeroPos + SideSize] );
	newState.zeroPos += SideSize;
	return newState;
}

ostream& operator << (ostream& out, const GameState& state)
{
	for( int y = 0; y < SideSize; y++ ) {
		for( int x = 0; x < SideSize; x++ ) {
			out << static_cast<int>(state.field[y*SideSize + x]) << " ";
		}
		out << endl;
	}
	out << endl;
	return out;
}

struct Hasher {
	size_t operator()(const GameState& state) const {
		size_t hash = 0;
		memcpy(&hash, &state.field[0], sizeof(size_t));
		return hash;
	}
};

string Get8thSolution(const GameState& state)
{
	queue<GameState> bfsQueue;
	bfsQueue.push(state);
	unordered_map<GameState, char, Hasher> visited;
	visited[state] = 'S';
	bool hasSolution = false;
	while( bfsQueue.size() > 0 ) {
		GameState tempState = bfsQueue.front();
		bfsQueue.pop();
		if( tempState.IsFinish() ) {
			hasSolution = true;		
			break;
		}
		if( tempState.CanMoveLeft() ) {
			GameState newState = tempState.MoveLeft();
			if( visited.find(newState) == visited.end() ) {
				visited[newState] = 'L';
				bfsQueue.push(newState);
			}
		}
		if( tempState.CanMoveUp() ) {
			GameState newState = tempState.MoveUp();
			if( visited.find(newState) == visited.end() ) {
				visited[newState] = 'U';
				bfsQueue.push(newState);
			}
		}
		if( tempState.CanMoveRight() ) {
			GameState newState = tempState.MoveRight();
			if( visited.find(newState) == visited.end() ) {
				visited[newState] = 'R';
				bfsQueue.push(newState);
			}
		}
		if( tempState.CanMoveDown() ) {
			GameState newState = tempState.MoveDown();
			if( visited.find(newState) == visited.end() ) {
				visited[newState] = 'D';
				bfsQueue.push(newState);
			}
		}
	}
	assert( hasSolution );

	string solution;
	GameState tempState(FinishState);
	char direction = visited[tempState];
	while( direction != 'S' ) {
		switch( direction ) {
		case 'L':
			tempState = tempState.MoveRight();
			break;
		case 'U':
			tempState = tempState.MoveDown();
			break;
		case 'R':
			tempState = tempState.MoveLeft();
			break;
		case 'D':
			tempState = tempState.MoveUp();
			break;
		}
		solution += direction;
		direction = visited[tempState];
	}

	std::reverse(solution.begin(), solution.end());
	return solution;
}

int main()
{
	GameState state({1, 0, 2, 8, 7, 6, 3, 4, 5});
	cout << state;

	string solution = Get8thSolution(state);
	for( char direction: solution ) {
		switch( direction ) {
		case 'L':
			state = state.MoveLeft();
			cout << state;
			break;
		case 'U':
			state = state.MoveUp();
			cout << state;
			break;
		case 'R':
			state = state.MoveRight();
			cout << state;
			break;
		case 'D':
			state = state.MoveDown();
			cout << state;
			break;
		default:
			assert( false );
		}
	}
	return 0;
}
