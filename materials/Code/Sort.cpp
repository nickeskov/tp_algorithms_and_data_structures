#include<iostream>
#include<vector>

struct Point {
	int X;
	int Y;
	Point() : X(0), Y(0) {}
};

// 1 
bool operator < (const Point& l, const Point& r)
{
	return l.X < r.X;
}

// 2
bool IsLessX(const Point& l, const Point& r)
{
	return l.X < r.X;
}

bool IsLessY(const Point& l, const Point& r)
{
	return l.Y < r.Y;
}

// 3
template<class T>
bool IsLessDefault(const T& l, const T& r)
{
	return l < r;
}

// 4
template<class T>
class IsLess {
public:
	bool operator()(const T& l, const T& r) { return l < r; }
};

// ���������� � ��������� [l, r)
template<class T, class Compare>
void mySort(T* arr, int l, int r, Compare cmp /*bool (isLess)(const T&, const T&) = IsLessDefault*/)
{
	for( int i = l; i < r; i++ )
		for( int j = l; j < r - 1; j++ )
			if( cmp(arr[j + 1], arr[j]) )
				std::swap(arr[j], arr[j + 1]);
}

int main()
{
	int n = 0;
	std::cin >> n;
	Point* arr = new Point[n];
	for( int i = 0; i < n; i++ )
		std::cin >> arr[i].X >> arr[i].Y;

	mySort(arr, 0, n, [](const Point& l, const Point& r) { return l.X < r.X; });
	std::cout << std::endl;
	
	std::vector<int> arr2;
	mySort(&arr2[0], 0, n, [](const int& l, const int& r) { return l < r; });
	
	for( int i = 0; i < n; i++ )
		std::cout << "(" << arr[i].X << ", " << arr[i].Y << ") ";
	return 0;
}
