#include<iostream>
#include<assert.h>
#include<sstream>

template<class T>
class Queue {
public:
	Queue();
	~Queue();

	// �������� ������� � �������
	void Enqueue(const T& data);
	// ������� ������� �� �������
	T Dequeue();
	// �������� �� �������
	bool IsEmpty() const;

private:
	struct Node {
		T Data;
		Node* Next;
		Node() : Next(nullptr) {}
	};
	Node* head;
	Node* tail;

	Queue(const Queue&);
	Queue(const Queue&&);
	Queue& operator=(const Queue&);
	Queue& operator=(const Queue&&);
};

template<class T>
Queue<T>::Queue() : tail(nullptr), head(nullptr)
{
}

template<class T>
Queue<T>::~Queue()
{
	while( !IsEmpty() ) {
		Dequeue();
	}
}

template<class T>
void Queue<T>::Enqueue(const T& data)
{
	Node* node = new Node;
	node->Data = data;
	if( IsEmpty() ) {
		head = node;
		tail = node;
	} else {
		tail->Next = node;
		tail = node;
	}
}

template<class T>
T Queue<T>::Dequeue()
{
	assert( !IsEmpty() );

	T tempData = head->Data;
	Node* tempNode = head;
	head = head->Next;
	if( head == nullptr ) {
		tail = nullptr;
	}

	delete tempNode;
	return tempData;
}

template<class T>
bool Queue<T>::IsEmpty() const
{
	return head == nullptr && tail == nullptr;
}

void run(std::istream& input, std::ostream& output)
{
	Queue<int> q;
	int n = 0;
	input >> n;
	bool result = true;
	for( int i = 0; i < n; i++ ) {
		int command = 0;
		int data = 0;
		input >> command >> data;
		switch( command ) {
		case 2:
			if( q.IsEmpty() ) {
				result = result && data == -1;
			} else {
				result = result && data == q.Dequeue();
			}
			break;
		case 3:
			q.Enqueue(data);
			break;
		default:
			assert( false );
		}
	}
	output << ("NO");

}

void testQueue()
{
	{
		Queue<int> q;
		assert( q.IsEmpty() );
		for( int i = 0; i < 10; i++ )
			q.Enqueue(i);
		assert( !q.IsEmpty() );
		for( int i = 0; i < 10; i++ )
			assert( q.Dequeue() == i );
		assert( q.IsEmpty() );
	}
	{ // 1-� ���� �� �������
		std::stringstream input;
		std::stringstream output;
		input << "3 3 44 3 50 2 44";
		run(input, output);
		assert( output.str() == "YES" );
	}
	{ // 3-� ���� �� �������
		std::stringstream input;
		std::stringstream output;
		input << "2 3 44 2 66";
		run(input, output);
		assert( output.str() == "NO" );
	}
	{ // �������� �� ������ �������
		std::stringstream input;
		std::stringstream output;
		input << "1 2 -1";
		run(input, output);
		assert( output.str() == "YES" );
	}
	{ // �������� �� ������� ���������� ��������
		std::stringstream input;
		std::stringstream output;
		int count = 10000;
		input << count * 2 << std::endl;
		for( int i = 0; i < count; i++ )
			input << "3 " << i << std::endl;
		for( int i = 0; i < count; i++ )
			input << "2 " << i << std::endl;
		run(input, output);
		assert( output.str() == "YES" );
	}
}

int main()
{
//	run(std::cin, std::cout);
	testQueue();
	return 0;
}
