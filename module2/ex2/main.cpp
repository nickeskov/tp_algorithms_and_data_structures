#ifndef DEFAULT_COMPARATOR_H
#define DEFAULT_COMPARATOR_H

template <typename T>
class DefaultComparator
{
  public:
    bool operator()(const T& lhs, const T& rhs)
    {
        return (lhs < rhs);
    }
};

#endif /* DEFAULT_COMPARATOR_H */
#ifndef FT_VECTOR_H
# define FT_VECTOR_H

#include <algorithm>
#include <utility>
#include <stdexcept>

template <typename T>
class FtVector
{
  public:

    FtVector(): m_size(0), m_capacity(0), m_data(nullptr) {}

    explicit FtVector(std::size_t init_size)
        : m_size(0)
        , m_capacity(init_size)
        , m_data(init_size ? new T[init_size] : nullptr) {}

    FtVector(const FtVector& other)
        : m_size(other.m_size)
        , m_capacity(other.m_capacity)
        , m_data(other.m_capacity ? new T[other.m_capacity] : nullptr)

    {
        std::copy(other.begin(), other.end(), begin());
    }

    FtVector(FtVector&& other) noexcept
        : m_size(other.m_size)
        , m_capacity(other.m_capacity)
        , m_data(other.m_data)
    {
        other.m_size = 0;
        other.m_capacity = 0;
        other.m_data = nullptr;
    }

    FtVector&   operator=(const FtVector& other)
    {
        if (this == &other)
            return (*this);

        delete[] m_data;
        m_data = new T[other.m_capacity];
        m_capacity = other.m_capacity;
        m_size = other.m_size;
        std::copy(other.begin(), other.end(), begin());
        return (*this);
    }

    FtVector&   operator=(FtVector&& other) noexcept
    {
        if (this == &other)
            return (*this);

        delete[] m_data;
        m_size = other.m_size;
        m_capacity = other.m_capacity;
        m_data = other.m_data;

        other.m_size = 0;
        other.m_capacity = 0;
        other.m_data = nullptr;

        return (*this);
    }

    void        push_back(const T& elem)
    {
        if (m_size == m_capacity)
        {
            std::size_t new_capacity;
            new_capacity = (m_size ?
                m_size * mc_capacity_increase_rate :
                mc_capacity_increase_rate);
            reserve(new_capacity);
        }
        m_data[m_size] = elem;
        ++m_size;
    }

    void        push_back(T&& elem)
    {
        if (m_size == m_capacity)
        {
            std::size_t new_capacity;
            new_capacity = (m_size ?
                m_size * mc_capacity_increase_rate :
                mc_capacity_increase_rate);
            reserve(new_capacity);
        }
        m_data[m_size] = std::move(elem);
        ++m_size;
    }

    T&          front()
    {
        if (m_data && m_size)
            return (*m_data);
        else
            throw (std::out_of_range("front"));
    }

    const T&    front() const
    {
        if (m_data && m_size)
            return (*m_data);
        else
            throw (std::out_of_range("front"));
    }

    T&          back()
    {
        if (m_data && m_size)
            return (m_data[m_size - 1]);
        else
            throw (std::out_of_range("back"));
    }

    const T&    back() const
    {
        if (m_data && m_size)
            return (m_data[m_size - 1]);
        else
            throw (std::out_of_range("back"));
    }

    void        pop_back()
    {
        if (m_data && m_size)
            --m_size;
        else
            throw (std::out_of_range("pop_back"));
    }

    T&          at(std::size_t pos)
    {
        if (m_data && pos < m_size)
            return (m_data[pos]);
        else
            throw (std::out_of_range("at"));
    }

    const T&    at(std::size_t pos) const
    {
        if (m_data && pos < m_size)
            return (m_data[pos]);
        else
            throw (std::out_of_range("at"));
    }

    T&          operator[](std::size_t pos)
    {
        return (at(pos));
    }

    const T&    operator[](std::size_t pos) const
    {
        return (at(pos));
    }

    void        resize(std::size_t new_size)
    {
        const T value = T();
        resize(new_size, value);
    }

    void        resize(std::size_t new_size, const T& value)
    {
        reserve(new_size);

        for (size_t i = m_size; i < new_size; ++i)
            m_data[i] = value;

        m_size = new_size;
   }

    void        reserve(std::size_t new_capacity)
    {
        if (new_capacity > m_capacity)
        {
            T   *new_data = new T[new_capacity];

            std::copy(begin(), end(), new_data);
            delete[] m_data;

            m_capacity = new_capacity;
            m_data = new_data;
        }
    }

    std::size_t size() const noexcept
    {
        return (m_size);
    }

    bool        empty() const noexcept
    {
        return (m_size == 0);
    }

    T*          begin() noexcept
    {
        return (m_data);
    }

    const T*    begin() const noexcept
    {
        return (m_data);
    }

    T*          end() noexcept
    {
        return (m_data + m_size);
    }

    const T*    end() const noexcept
    {
        return (m_data + m_size);
    }

    T*          data() noexcept
    {
        return (m_data);
    }

    const T*    data() const noexcept
    {
        return (m_data);
    }

    void        clear() noexcept
    {
        m_size = 0;
    }

    void        shrink_to_fit()
    {
        if (m_size != m_capacity)
        {
            T   *new_data = new T[m_size];
            std::copy(begin(), end(), new_data);
            delete[] m_data;

            m_data = new_data;
            m_capacity = m_size;
        }
    }

    void        reverse()
    {
        if (!m_size)
            return ;

        size_t i = 0;
        size_t j = m_size - 1;

        while (i < j)
        {
            std::swap(m_data[i], m_data[j]);
            ++i;
            --j;
        }
    }

    virtual
    ~FtVector()
    {
        delete[] m_data;
        m_data = nullptr;
    }

    void        unsafe_resize(std::size_t new_size)
    {
        reserve(new_size);
        m_size = new_size;
    }

  private:

    std::size_t         m_size;
    std::size_t         m_capacity;
    T                   *m_data;
    const std::size_t   mc_capacity_increase_rate = 2;
};

#endif /* FT_VECTOR_H */
#ifndef TREE_NODE_H
# define TREE_NODE_H

// # include <utility>

// # include "default_comparator.h"

template <typename T>
struct TreeNode
{
  public:
    TreeNode*   left = nullptr;
    TreeNode*   right = nullptr;
    TreeNode*   parent = nullptr;
    T           data;

    TreeNode() = default;
    TreeNode(const T& other): data(other) {}
    TreeNode(T&& other): data(std::forward<T>(other)) {}
};

#endif
#ifndef BHS_H
# define BHS_H

# include <memory>
# include <utility>
# include <cstddef>
# include <ostream>

// # include "default_comparator.h"
// # include "ft_vector.h"
// # include "tree_node.h"

template <typename T = long, typename Comparator = DefaultComparator<T>>
class BinarySearchTree
{
  public:
    BinarySearchTree() = default;
    BinarySearchTree(const BinarySearchTree&) = delete;
    BinarySearchTree& operator=(const BinarySearchTree& other) = delete;

    void        insert(const T& data);
    // void        insert(T&& data);

    void        print_inorder(std::ostream& os);

    std::size_t size()
    {
        return m_count;
    }

    ~BinarySearchTree();

  private:
    TreeNode<T> *m_root = nullptr;
    Comparator  m_cmp;
    std::size_t m_count;
};

template <typename T, typename Comparator>
void    BinarySearchTree<T, Comparator>::insert(const T& data)
{
    if (m_root)
    {
        TreeNode<T>* ptr = m_root;
        TreeNode<T>* ptr_parent = nullptr;

        while (ptr)
        {
            ptr_parent = ptr;
            ptr = m_cmp(data, ptr->data) ? ptr->left : ptr->right;
        }

        TreeNode<T> *new_node = new TreeNode<T>(data);

        new_node->parent = ptr_parent;

        if (m_cmp(data, ptr_parent->data))
        {
            ptr_parent->left = new_node;
        }
        else
        {
            ptr_parent->right = new_node;
        }
    }
    else
    {
        m_root = new TreeNode<T>(data);
    }
    ++m_count;
}

template <typename T, typename Comparator>
void    BinarySearchTree<T, Comparator>::print_inorder(std::ostream& os)
{
    if (!m_root)
        return ;

    FtVector<TreeNode<T>*> stack;
    TreeNode<T>* ptr = m_root;

    while (ptr || !stack.empty())
    {
        while (ptr)
        {
            stack.push_back(ptr);
            ptr = ptr->left;
        }
        ptr = stack.back();
        stack.pop_back();

        os << ptr->data << std::endl;
        ptr = ptr->right;
    }
}

template <typename T, typename Comparator>
BinarySearchTree<T, Comparator>::~BinarySearchTree()
{
    if (!m_root)
        return ;

    TreeNode<T>* ptr = m_root;
    FtVector<TreeNode<T>*> stack;

    while (ptr || !stack.empty())
    {
        while (ptr)
        {
            stack.push_back(ptr);
            ptr = ptr->left;
        }
        ptr = stack.back();
        stack.pop_back();

        TreeNode<T> *right = ptr->right;
        delete ptr;
        ptr = right;
    }
}

#endif
#include <iostream>

// #include "default_comparator.h"
// #include "bst.h"

int     main(void)
{
    std::size_t n = 0;

    std::cin >> n;
    BinarySearchTree<long, DefaultComparator<long>> bst;
    for (std::size_t i = 0; i < n; ++i)
    {
        long tmp;
        std::cin >> tmp;
        bst.insert(tmp);
    }
    bst.print_inorder(std::cout);
    return 0;
}
