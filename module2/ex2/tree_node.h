#ifndef TREE_NODE_H
# define TREE_NODE_H

# include <utility>

# include "default_comparator.h"

template <typename T>
struct TreeNode
{
  public:
    TreeNode*   left = nullptr;
    TreeNode*   right = nullptr;
    TreeNode*   parent = nullptr;
    T           data;

    TreeNode() = default;
    TreeNode(const T& other): data(other) {}
    TreeNode(T&& other): data(std::forward<T>(other)) {}
};

#endif
