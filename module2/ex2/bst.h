#ifndef BHS_H
# define BHS_H

# include <memory>
# include <utility>
# include <cstddef>
# include <ostream>

# include "default_comparator.h"
# include "ft_vector.h"
# include "tree_node.h"

template <typename T = long, typename Comparator = DefaultComparator<T>>
class BinarySearchTree
{
  public:
    BinarySearchTree() = default;
    BinarySearchTree(const BinarySearchTree&) = delete;
    BinarySearchTree& operator=(const BinarySearchTree& other) = delete;

    void        insert(const T& data);
    // void        insert(T&& data);

    void        print_inorder(std::ostream& os);

    std::size_t size()
    {
        return m_count;
    }

    ~BinarySearchTree();

  private:
    TreeNode<T> *m_root = nullptr;
    Comparator  m_cmp;
    std::size_t m_count;
};

template <typename T, typename Comparator>
void    BinarySearchTree<T, Comparator>::insert(const T& data)
{
    if (m_root)
    {
        TreeNode<T>* ptr = m_root;
        TreeNode<T>* ptr_parent = nullptr;

        while (ptr)
        {
            ptr_parent = ptr;
            ptr = m_cmp(data, ptr->data) ? ptr->left : ptr->right;
        }

        TreeNode<T> *new_node = new TreeNode<T>(data);

        new_node->parent = ptr_parent;

        if (m_cmp(data, ptr_parent->data))
        {
            ptr_parent->left = new_node;
        }
        else
        {
            ptr_parent->right = new_node;
        }
    }
    else
    {
        m_root = new TreeNode<T>(data);
    }
    ++m_count;
}

template <typename T, typename Comparator>
void    BinarySearchTree<T, Comparator>::print_inorder(std::ostream& os)
{
    if (!m_root)
        return ;

    FtVector<TreeNode<T>*> stack;
    TreeNode<T>* ptr = m_root;

    while (ptr || !stack.empty())
    {
        while (ptr)
        {
            stack.push_back(ptr);
            ptr = ptr->left;
        }
        ptr = stack.back();
        stack.pop_back();

        os << ptr->data << std::endl;
        ptr = ptr->right;
    }
}

template <typename T, typename Comparator>
BinarySearchTree<T, Comparator>::~BinarySearchTree()
{
    if (!m_root)
        return ;

    TreeNode<T>* ptr = m_root;
    FtVector<TreeNode<T>*> stack;

    while (ptr || !stack.empty())
    {
        while (ptr)
        {
            stack.push_back(ptr);
            ptr = ptr->left;
        }
        ptr = stack.back();
        stack.pop_back();

        TreeNode<T> *right = ptr->right;
        delete ptr;
        ptr = right;
    }
}

#endif
