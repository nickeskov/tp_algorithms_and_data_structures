#include <iostream>

#include "default_comparator.h"
#include "bst.h"

int     main(void)
{
    std::size_t n = 0;

    std::cin >> n;
    BinarySearchTree<long, DefaultComparator<long>> bst;
    for (std::size_t i = 0; i < n; ++i)
    {
        long tmp;
        std::cin >> tmp;
        bst.insert(tmp);
    }
    bst.print_inorder(std::cout);
    return 0;
}
