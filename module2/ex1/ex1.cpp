#include <iostream>
#include <string>

#include "ft_unordered_set.h"
#include "default_string_hash.h"


int    main()
{
    FtUnorderedSet<std::string, DefaultStringHash<std::string>> ft_uset;

    std::string key;
    std::string opp;

    while (std::cin >> opp >> key)
    {
        bool opp_status = false;
        if (opp == "+")
            opp_status = ft_uset.insert(std::move(key));
        else if (opp == "-")
            opp_status = ft_uset.erase(key);
        else if (opp == "?")
            opp_status = ft_uset.find(key);
        else
        {
            std::cout << "Unrecognized command" << std::endl;
            return 1;
        }
        std::cout << (opp_status ? "OK" : "FAIL") << std::endl;
    }
    return 0;
}
