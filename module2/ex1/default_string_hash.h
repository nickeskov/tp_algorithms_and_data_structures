#ifndef DEFAULT_STRING_HASH_H
#define DEFAULT_STRING_HASH_H

namespace {
    const std::size_t hash_param = 31;
}

template <typename T>
class DefaultStringHash
{
  public:
    std::size_t operator()(const T& str, std::size_t buckets_count) const
    {
        std::size_t hash = 0;
        for (std::size_t i = 0; str[i] != '\0'; ++i)
            hash = (hash * ::hash_param + str[i]) % buckets_count;
        return hash;
    }
};

#endif /* DEFAULT_STRING_HASH_H */
