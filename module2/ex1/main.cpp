#ifndef FT_VECTOR_H
# define FT_VECTOR_H

#include <algorithm>
#include <utility>
#include <stdexcept>

template <typename T>
class FtVector
{
  public:

    FtVector(): m_size(0), m_capacity(0), m_data(nullptr) {}

    explicit FtVector(std::size_t init_size)
        : m_size(0)
        , m_capacity(init_size)
        , m_data(init_size ? new T[init_size] : nullptr) {}

    FtVector(const FtVector& other)
        : m_size(other.m_size)
        , m_capacity(other.m_capacity)
        , m_data(other.m_capacity ? new T[other.m_capacity] : nullptr)

    {
        std::copy(other.begin(), other.end(), begin());
    }

    FtVector(FtVector&& other) noexcept
        : m_size(other.m_size)
        , m_capacity(other.m_capacity)
        , m_data(other.m_data)
    {
        other.m_size = 0;
        other.m_capacity = 0;
        other.m_data = nullptr;
    }

    FtVector&   operator=(const FtVector& other)
    {
        if (this == &other)
            return (*this);

        delete[] m_data;
        m_data = new T[other.m_capacity];
        m_capacity = other.m_capacity;
        m_size = other.m_size;
        std::copy(other.begin(), other.end(), begin());
        return (*this);
    }

    FtVector&   operator=(FtVector&& other) noexcept
    {
        if (this == &other)
            return (*this);

        delete[] m_data;
        m_size = other.m_size;
        m_capacity = other.m_capacity;
        m_data = other.m_data;

        other.m_size = 0;
        other.m_capacity = 0;
        other.m_data = nullptr;

        return (*this);
    }

    void        push_back(const T& elem)
    {
        if (m_size == m_capacity)
        {
            std::size_t new_capacity;
            new_capacity = (m_size ?
                m_size * mc_capacity_increase_rate :
                mc_capacity_increase_rate);
            reserve(new_capacity);
        }
        m_data[m_size] = elem;
        ++m_size;
    }

    void        push_back(T&& elem)
    {
        if (m_size == m_capacity)
        {
            std::size_t new_capacity;
            new_capacity = (m_size ?
                m_size * mc_capacity_increase_rate :
                mc_capacity_increase_rate);
            reserve(new_capacity);
        }
        m_data[m_size] = std::move(elem);
        ++m_size;
    }

    T&          front()
    {
        if (m_data && m_size)
            return (*m_data);
        else
            throw (std::out_of_range("front"));
    }

    const T&    front() const
    {
        if (m_data && m_size)
            return (*m_data);
        else
            throw (std::out_of_range("front"));
    }

    T&          back()
    {
        if (m_data && m_size)
            return (m_data[m_size - 1]);
        else
            throw (std::out_of_range("back"));
    }

    const T&    back() const
    {
        if (m_data && m_size)
            return (m_data[m_size - 1]);
        else
            throw (std::out_of_range("back"));
    }

    void        pop_back()
    {
        if (m_data && m_size)
            --m_size;
        else
            throw (std::out_of_range("pop_back"));
    }

    T&          at(std::size_t pos)
    {
        if (m_data && pos < m_size)
            return (m_data[pos]);
        else
            throw (std::out_of_range("at"));
    }

    const T&    at(std::size_t pos) const
    {
        if (m_data && pos < m_size)
            return (m_data[pos]);
        else
            throw (std::out_of_range("at"));
    }

    T&          operator[](std::size_t pos)
    {
        return (at(pos));
    }

    const T&    operator[](std::size_t pos) const
    {
        return (at(pos));
    }

    void        resize(std::size_t new_size)
    {
        const T value = T();
        resize(new_size, value);
    }

    void        resize(std::size_t new_size, const T& value)
    {
        reserve(new_size);

        for (size_t i = m_size; i < new_size; ++i)
            m_data[i] = value;

        m_size = new_size;
   }

    void        reserve(std::size_t new_capacity)
    {
        if (new_capacity > m_capacity)
        {
            T   *new_data = new T[new_capacity];

            std::copy(begin(), end(), new_data);
            delete[] m_data;

            m_capacity = new_capacity;
            m_data = new_data;
        }
    }

    std::size_t size() const noexcept
    {
        return (m_size);
    }

    bool        empty() const noexcept
    {
        return (m_size == 0);
    }

    T*          begin() noexcept
    {
        return (m_data);
    }

    const T*    begin() const noexcept
    {
        return (m_data);
    }

    T*          end() noexcept
    {
        return (m_data + m_size);
    }

    const T*    end() const noexcept
    {
        return (m_data + m_size);
    }

    T*          data() noexcept
    {
        return (m_data);
    }

    const T*    data() const noexcept
    {
        return (m_data);
    }

    void        clear() noexcept
    {
        m_size = 0;
    }

    void        shrink_to_fit()
    {
        if (m_size != m_capacity)
        {
            T   *new_data = new T[m_size];
            std::copy(begin(), end(), new_data);
            delete[] m_data;

            m_data = new_data;
            m_capacity = m_size;
        }
    }

    void        reverse()
    {
        if (!m_size)
            return ;

        size_t i = 0;
        size_t j = m_size - 1;

        while (i < j)
        {
            std::swap(m_data[i], m_data[j]);
            ++i;
            --j;
        }
    }

    virtual
    ~FtVector()
    {
        delete[] m_data;
        m_data = nullptr;
    }

    void        unsafe_resize(std::size_t new_size)
    {
        reserve(new_size);
        m_size = new_size;
    }

  private:

    std::size_t         m_size;
    std::size_t         m_capacity;
    T                   *m_data;
    const std::size_t   mc_capacity_increase_rate = 2;
};

#endif /* FT_VECTOR_H */
#ifndef FT_UNORDERED_SET_H
#define FT_UNORDERED_SET_H

#include <stdexcept>
#include <exception>
#include <limits>
#include <utility>

//#include "ft_vector.h"


//template <typename Key>
//class IProbingPolicy
//{
//  public:
//    virtual
//    std::size_t operator()(std::size_t probes_count) = 0;
//};
//
//template <
//        typename Key,
//        typename Hash >
//class QuadraticProbingPolicy : public IProbingPolicy<Key>
//{
//  public:
//    explicit
//    QuadraticProbingPolicy(const Key &key, std::size_t buckets_count)
//        : m_buckets_count(buckets_count)
//        , m_hasher(Hash()),
//         m_last_call_hash(m_hasher(key, buckets_count)) {}
//
//    std::size_t operator()(std::size_t probes_count) override
//    {
//        m_last_call_hash += probes_count % m_buckets_count;
//        return m_last_call_hash;
//    }
//
//private:
//    std::size_t m_buckets_count;
//    std::size_t m_last_call_hash = std::numeric_limits<std::size_t>::max();
//    Hash        m_hasher;
//};


namespace {
    const std::size_t unordered_set_inc_rate = 2;
}


enum class BucketStatus
{
    Nil = 0u,
    Deleted,
    Used
};

template <typename Key>
struct Bucket
{
    Bucket() = default;

    explicit
    Bucket(const Key &key, BucketStatus status = BucketStatus::Nil)
        : key(key)
        , status(status) {}

    explicit
    Bucket(Key &&key, BucketStatus status = BucketStatus::Nil)
        : key(std::forward<Key>(key))
        , status(status) {}

    Key           key{};
    BucketStatus  status{};
};


template <
    typename Key,
    typename Hash,
    typename KeyEqual = std::equal_to<Key> >
class FtUnorderedSet
{
  public:
    const Hash      hasher = Hash();
    const KeyEqual  key_equal = KeyEqual();

    double load_factor() const;
    double max_load_factor() const;
    void max_load_factor(double ml);

    bool insert(const Key &key);
    bool insert(Key &&key);
    bool find(const Key &key) const;
    bool erase(const Key &key);
    void rehash(std::size_t count);

    explicit
    FtUnorderedSet(std::size_t init_size = 8)
        : m_buckets_count(init_size < 8 ? 8 : init_size)
        , m_buckets(FtVector<Bucket<Key>>(m_buckets_count))
    {
        m_buckets.unsafe_resize(m_buckets_count);
    }

  private:
    std::size_t             m_buckets_count;
    std::size_t             m_size = 0;
    FtVector<Bucket<Key>>   m_buckets;
    float                   m_max_load_factor = 0.75;

    bool insert_bucket(Bucket<Key> &&bucket);
};

template<typename Key, typename Hash, typename KeyEqual>
double FtUnorderedSet<Key, Hash, KeyEqual>::load_factor() const
{
    return ((double) m_size) / m_buckets_count;
}

template<typename Key, typename Hash, typename KeyEqual>
double FtUnorderedSet<Key, Hash, KeyEqual>::max_load_factor() const
{
    return m_max_load_factor;
}

template<typename Key, typename Hash, typename KeyEqual>
void FtUnorderedSet<Key, Hash, KeyEqual>::max_load_factor(double ml)
{
    if (ml <= std::numeric_limits<double>::epsilon() || ml >= 1.0)
        throw std::invalid_argument("max_load_factor <=0 || max_load_factor >= 1.0");
    m_max_load_factor = ml;
}

template<typename Key, typename Hash, typename KeyEqual>
bool FtUnorderedSet<Key, Hash, KeyEqual>::insert(const Key &key)
{
    return insert_bucket(Bucket<Key>(key, BucketStatus::Used));
}

template<typename Key, typename Hash, typename KeyEqual>
bool FtUnorderedSet<Key, Hash, KeyEqual>::insert(Key &&key)
{
    return insert_bucket(Bucket<Key>(std::forward<Key>(key), BucketStatus::Used));
}

template<typename Key, typename Hash, typename KeyEqual>
bool FtUnorderedSet<Key, Hash, KeyEqual>::find(const Key &key) const
{
    bool is_found_successfully = false;
    auto pos = hasher(key, m_buckets_count);

    for (std::size_t i = 0;
        i < m_buckets_count && m_buckets[pos].status != BucketStatus::Nil;
        ++i)
    {
        if (key_equal(key, m_buckets[pos].key))
        {
            is_found_successfully = m_buckets[pos].status == BucketStatus::Used;
            break ;
        }
        pos = (pos + i + 1) % m_buckets_count;
    }
    return is_found_successfully;
}

template<typename Key, typename Hash, typename KeyEqual>
bool FtUnorderedSet<Key, Hash, KeyEqual>::erase(const Key &key)
{
    bool is_deleted_successfully = false;
    auto pos = hasher(key, m_buckets_count);

    if (find(key))
    {
        for (std::size_t i = 0;
             i < m_buckets_count && m_buckets[pos].status != BucketStatus::Nil;
             ++i)
        {
            if (key_equal(key, m_buckets[pos].key))
            {
                m_buckets[pos].status = BucketStatus::Deleted;
                --m_size;
                is_deleted_successfully = true;
                break ;
            }
            pos = (pos + i + 1) % m_buckets_count;
        }
    }
    return is_deleted_successfully;
}

template<typename Key, typename Hash, typename KeyEqual>
void FtUnorderedSet<Key, Hash, KeyEqual>::rehash(std::size_t count)
{
    if (((double) m_size) / count > max_load_factor())
        throw std::logic_error("rehash and set buckets_count to the smaller value");
    FtVector<Bucket<Key>> new_buckets(count);
    new_buckets.unsafe_resize(count);
    FtVector<Bucket<Key>> old_buckets = std::move(m_buckets);

    m_buckets = std::move(new_buckets);
    m_buckets_count = count;
    m_size = 0;

    for (std::size_t i = 0; i < old_buckets.size(); ++i)
    {
        if (old_buckets[i].status == BucketStatus::Used)
            insert_bucket(std::move(old_buckets[i]));
    }
}

template<typename Key, typename Hash, typename KeyEqual>
bool FtUnorderedSet<Key, Hash, KeyEqual>::insert_bucket(Bucket<Key> &&bucket) {
    bool is_inserted_successfully = false;

    if (load_factor() >= max_load_factor())
        rehash(m_buckets_count * ::unordered_set_inc_rate);

    if (!find(bucket.key))
    {
        auto pos = hasher(bucket.key, m_buckets_count);
        for (std::size_t i = 0; i < m_buckets_count; ++i)
        {
            if (m_buckets[pos].status != BucketStatus::Used)
            {
                m_buckets[pos] = std::forward<Bucket<Key>>(bucket);
                ++m_size;
                is_inserted_successfully = true;
                break ;
            }
            pos = (pos + i + 1) % m_buckets_count;
        }
    }
    return is_inserted_successfully;
}

#endif /* FT_UNORDERED_SET_H */
#ifndef DEFAULT_STRING_HASH_H
#define DEFAULT_STRING_HASH_H

namespace {
    const std::size_t hash_param = 31;
}

template <typename T>
class DefaultStringHash
{
  public:
    std::size_t operator()(const T& str, std::size_t buckets_count) const
    {
        std::size_t hash = 0;
        for (std::size_t i = 0; str[i] != '\0'; ++i)
            hash = (hash * ::hash_param + str[i]) % buckets_count;
        return hash;
    }
};

#endif /* DEFAULT_STRING_HASH_H */
#include <iostream>
#include <string>

//#include "ft_unordered_set.h"
//#include "default_string_hash.h"


int    main()
{
    FtUnorderedSet<std::string, DefaultStringHash<std::string>> ft_uset;

    std::string key;
    std::string opp;

    while (std::cin >> opp >> key)
    {
        bool opp_status = false;
        if (opp == "+")
            opp_status = ft_uset.insert(std::move(key));
        else if (opp == "-")
            opp_status = ft_uset.erase(key);
        else if (opp == "?")
            opp_status = ft_uset.find(key);
        else
        {
            std::cout << "Unrecognized command" << std::endl;
            return 1;
        }
        std::cout << (opp_status ? "OK" : "FAIL") << std::endl;
    }
    return 0;
}
