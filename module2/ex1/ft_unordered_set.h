#ifndef FT_UNORDERED_SET_H
#define FT_UNORDERED_SET_H

#include <stdexcept>
#include <exception>
#include <limits>
#include <utility>

#include "ft_vector.h"


//template <typename Key>
//class IProbingPolicy
//{
//  public:
//    virtual
//    std::size_t operator()(std::size_t probes_count) = 0;
//};
//
//template <
//        typename Key,
//        typename Hash >
//class QuadraticProbingPolicy : public IProbingPolicy<Key>
//{
//  public:
//    explicit
//    QuadraticProbingPolicy(const Key &key, std::size_t buckets_count)
//        : m_buckets_count(buckets_count)
//        , m_hasher(Hash()),
//         m_last_call_hash(m_hasher(key, buckets_count)) {}
//
//    std::size_t operator()(std::size_t probes_count) override
//    {
//        m_last_call_hash += probes_count % m_buckets_count;
//        return m_last_call_hash;
//    }
//
//private:
//    std::size_t m_buckets_count;
//    std::size_t m_last_call_hash = std::numeric_limits<std::size_t>::max();
//    Hash        m_hasher;
//};


namespace {
    const std::size_t unordered_set_inc_rate = 2;
}


enum class BucketStatus
{
    Nil = 0u,
    Deleted,
    Used
};

template <typename Key>
struct Bucket
{
    Bucket() = default;

    explicit
    Bucket(const Key &key, BucketStatus status = BucketStatus::Nil)
        : key(key)
        , status(status) {}

    explicit
    Bucket(Key &&key, BucketStatus status = BucketStatus::Nil)
        : key(std::forward<Key>(key))
        , status(status) {}

    Key           key{};
    BucketStatus  status{};
};


template <
    typename Key,
    typename Hash,
    typename KeyEqual = std::equal_to<Key> >
class FtUnorderedSet
{
  public:
    const Hash      hasher = Hash();
    const KeyEqual  key_equal = KeyEqual();

    double load_factor() const;
    double max_load_factor() const;
    void max_load_factor(double ml);

    bool insert(const Key &key);
    bool insert(Key &&key);
    bool find(const Key &key) const;
    bool erase(const Key &key);
    void rehash(std::size_t count);

    explicit
    FtUnorderedSet(std::size_t init_size = 8)
        : m_buckets_count(init_size < 8 ? 8 : init_size)
        , m_buckets(FtVector<Bucket<Key>>(m_buckets_count))
    {
        m_buckets.unsafe_resize(m_buckets_count);
    }

  private:
    std::size_t             m_buckets_count;
    std::size_t             m_size = 0;
    FtVector<Bucket<Key>>   m_buckets;
    float                   m_max_load_factor = 0.75;

    bool insert_bucket(Bucket<Key> &&bucket);
};

template<typename Key, typename Hash, typename KeyEqual>
double FtUnorderedSet<Key, Hash, KeyEqual>::load_factor() const
{
    return ((double) m_size) / m_buckets_count;
}

template<typename Key, typename Hash, typename KeyEqual>
double FtUnorderedSet<Key, Hash, KeyEqual>::max_load_factor() const
{
    return m_max_load_factor;
}

template<typename Key, typename Hash, typename KeyEqual>
void FtUnorderedSet<Key, Hash, KeyEqual>::max_load_factor(double ml)
{
    if (ml <= std::numeric_limits<double>::epsilon() || ml >= 1.0)
        throw std::invalid_argument("max_load_factor <=0 || max_load_factor >= 1.0");
    m_max_load_factor = ml;
}

template<typename Key, typename Hash, typename KeyEqual>
bool FtUnorderedSet<Key, Hash, KeyEqual>::insert(const Key &key)
{
    return insert_bucket(Bucket<Key>(key, BucketStatus::Used));
}

template<typename Key, typename Hash, typename KeyEqual>
bool FtUnorderedSet<Key, Hash, KeyEqual>::insert(Key &&key)
{
    return insert_bucket(Bucket<Key>(std::forward<Key>(key), BucketStatus::Used));
}

template<typename Key, typename Hash, typename KeyEqual>
bool FtUnorderedSet<Key, Hash, KeyEqual>::find(const Key &key) const
{
    bool is_found_successfully = false;
    auto pos = hasher(key, m_buckets_count);

    for (std::size_t i = 0;
        i < m_buckets_count && m_buckets[pos].status != BucketStatus::Nil;
        ++i)
    {
        if (key_equal(key, m_buckets[pos].key))
        {
            is_found_successfully = m_buckets[pos].status == BucketStatus::Used;
            break ;
        }
        pos = (pos + i + 1) % m_buckets_count;
    }
    return is_found_successfully;
}

template<typename Key, typename Hash, typename KeyEqual>
bool FtUnorderedSet<Key, Hash, KeyEqual>::erase(const Key &key)
{
    bool is_deleted_successfully = false;
    auto pos = hasher(key, m_buckets_count);

    if (find(key))
    {
        for (std::size_t i = 0;
             i < m_buckets_count && m_buckets[pos].status != BucketStatus::Nil;
             ++i)
        {
            if (key_equal(key, m_buckets[pos].key))
            {
                m_buckets[pos].status = BucketStatus::Deleted;
                --m_size;
                is_deleted_successfully = true;
                break ;
            }
            pos = (pos + i + 1) % m_buckets_count;
        }
    }
    return is_deleted_successfully;
}

template<typename Key, typename Hash, typename KeyEqual>
void FtUnorderedSet<Key, Hash, KeyEqual>::rehash(std::size_t count)
{
    if (((double) m_size) / count > max_load_factor())
        throw std::logic_error("rehash and set buckets_count to the smaller value");
    FtVector<Bucket<Key>> new_buckets(count);
    new_buckets.unsafe_resize(count);
    FtVector<Bucket<Key>> old_buckets = std::move(m_buckets);

    m_buckets = std::move(new_buckets);
    m_buckets_count = count;
    m_size = 0;

    for (std::size_t i = 0; i < old_buckets.size(); ++i)
    {
        if (old_buckets[i].status == BucketStatus::Used)
            insert_bucket(std::move(old_buckets[i]));
    }
}

template<typename Key, typename Hash, typename KeyEqual>
bool FtUnorderedSet<Key, Hash, KeyEqual>::insert_bucket(Bucket<Key> &&bucket) {
    bool is_inserted_successfully = false;

    if (load_factor() >= max_load_factor())
        rehash(m_buckets_count * ::unordered_set_inc_rate);

    if (!find(bucket.key))
    {
        auto pos = hasher(bucket.key, m_buckets_count);
        for (std::size_t i = 0; i < m_buckets_count; ++i)
        {
            if (m_buckets[pos].status != BucketStatus::Used)
            {
                m_buckets[pos] = std::forward<Bucket<Key>>(bucket);
                ++m_size;
                is_inserted_successfully = true;
                break ;
            }
            pos = (pos + i + 1) % m_buckets_count;
        }
    }
    return is_inserted_successfully;
}

#endif /* FT_UNORDERED_SET_H */
