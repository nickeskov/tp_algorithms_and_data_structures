cmake_minimum_required(VERSION 3.10)

project(ex1)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0 -ggdb3 -Wall -Wextra -Werror")

add_executable(a.out ex1.cpp)
