import random

n = 10000

lst = [i for i in range(1, n + 1)]

random.Random(14956).shuffle(lst)

print(2 * n - 1)

for i in range(1, n + 1):
    print(lst[i - 1], random.randrange(i))

random.Random(5113).shuffle(lst)

k = n;
for i in range(1, n):
    k += -1
    print(-lst[i - 1], random.randrange(k))
