#ifndef TREE_NODE_H
# define TREE_NODE_H

# include <utility>

template <typename T>
struct AvlTreeNode
{
  public:
    T               data;
    AvlTreeNode*    left = nullptr;
    AvlTreeNode*    right = nullptr;
    AvlTreeNode*    parent = nullptr;
    int             height = 1;
    std::size_t     left_size = 0;
    std::size_t     right_size = 0;

    AvlTreeNode() = default;

    explicit
    AvlTreeNode(const T &data, AvlTreeNode *parent = nullptr)
        : data(data)
        , parent(parent) {}

    explicit
    AvlTreeNode(T &&data, AvlTreeNode *parent = nullptr)
        : data(std::forward<T>(data))
        , parent(parent) {}

    int balance_factor() const noexcept;

    void fixup() noexcept;

private:
    int         get_height() noexcept;

    std::size_t get_subtree_size(const AvlTreeNode *node) noexcept;
};

template<typename T>
int AvlTreeNode<T>::balance_factor() const noexcept
{
    const int left_height = left ? left->height : 0;
    const int right_height = right ? right->height : 0;
    return right_height - left_height;
}

template<typename T>
void AvlTreeNode<T>::fixup() noexcept
{
    height = get_height();
    left_size = get_subtree_size(left);
    right_size = get_subtree_size(right);
}

template<typename T>
int AvlTreeNode<T>::get_height() noexcept
{
    const int lsize = left ? left->height : 0;
    const int rsize = right ? right->height : 0;
    return (lsize > rsize ? lsize : rsize) + 1;
}

template<typename T>
std::size_t AvlTreeNode<T>::get_subtree_size(const AvlTreeNode *node) noexcept
{
    std::size_t subtree_size = 0;
    if (node)
        subtree_size = node->left_size + node->right_size + 1;
    return subtree_size;
}

#endif
