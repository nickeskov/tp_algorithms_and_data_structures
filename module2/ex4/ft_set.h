#ifndef FT_SET_H
#define FT_SET_H

#include <stdexcept>
#include <exception>
#include <limits>
#include <utility>
#include <ostream>

#include "default_comparator.h"
#include "ft_vector.h"
#include "avl_tree_node.h"

template <
        typename Key,
        typename Comparator = DefaultComparator<Key>,
        typename KeyEqual = std::equal_to<Key> >
class FtSet
{
  public:
    const Comparator cmp = Comparator();
    const KeyEqual key_equal = KeyEqual();

    typedef AvlTreeNode<Key> NodeType;

    FtSet() = default;
    FtSet(const FtSet&) = delete;
    FtSet& operator=(const FtSet&) = delete;

    void insert(const Key &key);

    bool has(const Key& key) const;

    std::size_t size() const noexcept;

    bool empty() const noexcept;

    Key *find(const Key &key) const;

    bool erase(const Key &key);

    const Key *get_kstat(std::size_t k) const noexcept;

    void print_inorder(std::ostream &os);

    void clear();

    ~FtSet();

  private:
    AvlTreeNode<Key> *m_root = nullptr;
    std::size_t m_size = 0;

    template <typename Procedure>
    void inorder_traverse(Procedure procedure);

    void rotate_right(NodeType *pnode);

    void rotate_left(NodeType *qnode);

    void balance(NodeType *node);

    NodeType *find_node(const Key &key) const;

    NodeType *find_min_node(NodeType *node) const;

    void delete_node(NodeType *&node);

};

template<typename Key, typename Comparator, typename KeyEqual>
template<typename Procedure>
void FtSet<Key, Comparator, KeyEqual>::inorder_traverse(Procedure procedure)
{
    if (!m_root)
        return ;

    FtVector<NodeType*> stack;
    NodeType* ptr = m_root;

    while (ptr || !stack.empty())
    {
        while (ptr)
        {
            stack.push_back(ptr);
            ptr = ptr->left;
        }
        ptr = stack.back();
        stack.pop_back();

        NodeType *right = ptr->right;

        procedure(ptr);

        ptr = right;
    }
}

template<typename Key, typename Comparator, typename KeyEqual>
void FtSet<Key, Comparator, KeyEqual>::print_inorder(std::ostream &os)
{
    inorder_traverse([&](const NodeType *ptr) {
        if (ptr)
            os << ptr->data << std::endl;
    });
}

template<typename Key, typename Comparator, typename KeyEqual>
void FtSet<Key, Comparator, KeyEqual>::clear()
{
    inorder_traverse([](NodeType *ptr) {
        delete ptr;
    });
    m_root = nullptr;
    m_size = 0;
}


template<typename Key, typename Comparator, typename KeyEqual>
FtSet<Key, Comparator, KeyEqual>::~FtSet()
{
    clear();
}

template<typename Key, typename Comparator, typename KeyEqual>
void FtSet<Key, Comparator, KeyEqual>::rotate_right(NodeType *pnode)
{
    if (!pnode)
        throw std::invalid_argument("[FATAL]: nullptr in rotate_right as a parameter");

    NodeType *qnode = pnode->left;
    if (!qnode)
        throw std::logic_error("[FATAL]: second required node for rotate_right is nullptr");

    pnode->left = qnode->right;
    qnode->right = pnode;

    qnode->parent = pnode->parent;
    pnode->parent = qnode;

    if (qnode->parent == nullptr)
        m_root = qnode;
    else if (qnode->parent->left == pnode)
        qnode->parent->left = qnode;
    else
        qnode->parent->right = qnode;

    if (pnode->left)
        pnode->left->parent = pnode;

    pnode->fixup();
    qnode->fixup();
}

template<typename Key, typename Comparator, typename KeyEqual>
void FtSet<Key, Comparator, KeyEqual>::rotate_left(NodeType *qnode)
{
    if (!qnode)
        throw std::invalid_argument("[FATAL]: nullptr in rotate_right as a parameter");

    NodeType *pnode = qnode->right;
    if (!pnode)
        throw std::logic_error("[FATAL]: second required node for rotate_left is nullptr");

    qnode->right = pnode->left;
    pnode->left = qnode;

    pnode->parent = qnode->parent;
    qnode->parent = pnode;

    if (pnode->parent == nullptr)
        m_root = pnode;
    else if (pnode->parent->left == qnode)
        pnode->parent->left = pnode;
    else
        pnode->parent->right = pnode;

    if (qnode->right)
        qnode->right->parent = qnode;

    qnode->fixup();
    pnode->fixup();
}


template<typename Key, typename Comparator, typename KeyEqual>
typename FtSet<Key, Comparator, KeyEqual>::NodeType
    *FtSet<Key, Comparator, KeyEqual>::find_min_node(NodeType *node) const
{
    if (!node)
        throw std::invalid_argument("[FATAL]: nullptr in find_min_node as a parameter");
    while (node->left)
        node = node->left;
    return node;
}

template<typename Key, typename Comparator, typename KeyEqual>
void FtSet<Key, Comparator, KeyEqual>::balance(NodeType *node)
{
    if (node == nullptr)
        return;

    node->fixup();

    auto node_bf = node->balance_factor();

    if (node_bf == 2)
    {
        if (node->right->balance_factor() == -1)
            rotate_right(node->right);
        rotate_left(node);
    }
    else if (node_bf == -2)
    {
        if (node->left->balance_factor() == 1)
            rotate_left(node->left);
        rotate_right(node);
    }
}

template<typename Key, typename Comparator, typename KeyEqual>
const Key *FtSet<Key, Comparator, KeyEqual>::get_kstat(std::size_t k) const noexcept
{
    NodeType *node = m_root;
    while (node && k != node->left_size)
    {
        if (node->left_size < k)
        {
            k -= node->left_size + 1;
            node = node->right;
        }
        else
            node = node->left;
    }
    return node ? &(node->data) : nullptr;
}

template<typename Key, typename Comparator, typename KeyEqual>
void FtSet<Key, Comparator, KeyEqual>::insert(const Key &key)
{
    NodeType *node = m_root;
    NodeType *node_parent = nullptr;

    while (node)
    {
        node_parent = node;
        node = cmp(key, node->data) ? node->left : node->right;
    }

    auto *new_node = new NodeType(key, node_parent);
    if (node_parent == nullptr)
        m_root = new_node;
    else if (cmp(key, node_parent->data))
        node_parent->left = new_node;
    else
        node_parent->right = new_node;

    node = node_parent;
    while (node)
    {
        balance(node);
        node = node->parent;
    }
    balance(m_root);
    m_size++;
}

template<typename Key, typename Comparator, typename KeyEqual>
Key *FtSet<Key, Comparator, KeyEqual>::find(const Key &key) const
{
    NodeType *node = find_node(key);
    return node ? &(node->data) : nullptr;
}

template<typename Key, typename Comparator, typename KeyEqual>
typename FtSet<Key, Comparator, KeyEqual>::NodeType
    *FtSet<Key, Comparator, KeyEqual>::find_node(const Key &key) const
{
    NodeType *node = m_root;
    while (node && !key_equal(key, node->data))
        node = cmp(key, node->data) ? node->left : node->right;
    return node;
}

template<typename Key, typename Comparator, typename KeyEqual>
bool FtSet<Key, Comparator, KeyEqual>::erase(const Key &key)
{
    NodeType *node = find_node(key);
    if (node == nullptr)
        return false;

    NodeType *parent_node = node->parent;

    if (parent_node == nullptr)
        delete_node(m_root);
    else if (cmp(key, parent_node->data))
        delete_node(parent_node->left);
    else
        delete_node(parent_node->right);

    node = parent_node;
    while (node)
    {
        balance(node);
        node = node->parent;
    }

    balance(m_root);
    return true;
}

template<typename Key, typename Comparator, typename KeyEqual>
void FtSet<Key, Comparator, KeyEqual>::delete_node(NodeType *&node)
{
    if (!node)
        throw std::invalid_argument("[FATAL]: nullptr in delete_node as a parameter");

    NodeType *parent_node = node->parent;
    NodeType *left = node->left;
    NodeType *right = node->right;

    if (left == nullptr)
    {
        delete node;

        node = right;
        if (right)
            right->parent = parent_node;
    }
    else if (right == nullptr)
    {
        delete node;

        node = left;
        left->parent = parent_node;
    }
    else
    {
        NodeType *min_node = find_min_node(right);

        node->data = std::move(min_node->data);

        if (min_node->parent->left == min_node)
        {
            min_node->parent->left = min_node->right;
            if (min_node->right)
                min_node->right->parent = min_node->parent;
        }
        else
        {
            min_node->parent->right = min_node->right;
            if (min_node->right)
                min_node->right->parent = min_node->parent;
        }

        NodeType *min_node_parent = min_node->parent;
        while (min_node_parent != node)
        {
            balance(min_node_parent);
            min_node_parent = min_node_parent->parent;
        }

        delete min_node;

        balance(node);
    }
    m_size--;
}

template<typename Key, typename Comparator, typename KeyEqual>
bool FtSet<Key, Comparator, KeyEqual>::has(const Key &key) const
{
    return find(key) != nullptr;
}

template<typename Key, typename Comparator, typename KeyEqual>
std::size_t FtSet<Key, Comparator, KeyEqual>::size() const noexcept
{
    return m_size;
}

template<typename Key, typename Comparator, typename KeyEqual>
bool FtSet<Key, Comparator, KeyEqual>::empty() const noexcept
{
    return m_size == 0;
}

#endif /* FT_SET_H */
