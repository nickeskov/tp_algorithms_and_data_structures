#include <iostream>

#include "ft_set.h"

int    main()
{
    std::size_t n = 0;

    std::cin >> n;
    FtSet<long> set;

    for (std::size_t i = 0; i < n; ++i)
    {
        long a, k;
        if (std::cin >> a >> k)
        {
            if (a > 0)
                set.insert(a);
            else {
                a *= -1;
                set.erase(a);
            }
            if (!set.empty())
                std::cout << *(set.get_kstat(k)) << std::endl;
        }
    }
    return 0;
}
